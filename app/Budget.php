<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Budget extends Model
{
    //use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'start_date',
        'end_date',
        'category',
        'title',
        'amount',
        'extend_amount',
        'comment',
        'saved_by',
    ];
}
