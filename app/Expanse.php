<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Expanse extends Model
{
    //use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'category',
        'head',
        'amount',
        'saved_by',
        'flag',
    ];

}
