<?php

namespace App;

use Illuminate\Database\Eloquent\Model;



class Loan extends Model
{
    //use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'tbl_loan';
    protected $fillable = ['account_no',
        'account_name',
        'bank_name',
        'bank_name',
        'bank_balance',
        'amount',
        'percent',
        'loan_duration',
        'loan_start_date',
        'comment',
        'saved_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    public function installments(){
        return $this->hasMany('App\Installments');
    }
}
