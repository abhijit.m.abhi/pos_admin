<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Income extends Model
{
    //use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'incomes';
    protected $fillable = [
        'date',
        'head',
        'amount',
        'saved_by',
        'flag',
    ];
}
