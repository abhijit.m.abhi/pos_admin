<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BudgetCategory extends Model
{
    protected $table = 'budgetcategory';
    protected $fillable = [
        'categoryName'
    ];
}
