<?php

namespace App\Http\Controllers;

use App\Http\Requests\AddExpanseRequest;
use App\Http\Requests\EditExpanseRequest;
use App\Expanse;
use App\newCategory;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;

use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;

class ExpanseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $allExpanseList = Expanse::latest()->get();
        return view('expanseList', ['allExpanseList' => $allExpanseList]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $categoryList = newCategory::all();
        return view('addExpanse', ['categoryList' => $categoryList]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(AddExpanseRequest $request)
    {
        $expanse = new Expanse();
        $expanse->category = $request->input('expanse_category');

        if ($request->input('type') != '') {
            $expanse->head = $request->input('type');
        }
        if ($request->input('staff_name') != '') {

            $expanse->head = $request->input('staff_name');
        }

        $expanse->amount = $request->input('amount');
        $expanse->saved_by = Auth::user()->id;

        //$expanse->date = Carbon::now()->format('d-m-Y');
        $expanse->date = date('Y-m-d');


        if ($expanse->save()) {
            //$request->session()->flash('status', 'success');
            Session::flash('message', 'Expense Added Successfully !');
        };

        return redirect('/expanse');

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $expanseData = Expanse::find($id);
        return view('editExpanse', ['expanse' => $expanseData]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(AddExpanseRequest $request, Expanse $expanse)
    {
        //
        $expanse->category = $request->input('category');
        $expanse->head = $request->input('head');
        $expanse->amount = $request->input('amount');


        if ($expanse->update()) {
            //$request->session()->flash('status', 'success');
            Session::flash('message', 'Information Updated Successfully !');
            return redirect('/expanse');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $expanse = Expanse::destroy($id);
    }

    function delete_expanse($id)
    {
        $user = Expanse::findOrFail($id);
        if ($user->delete()) {
            return redirect('/expanse');
        }
    }

    /**
     * Show Expense Report
     */
    function report()
    {

        $allExpanseList = Expanse::latest()->get();
        return view('expanseReport', ['allExpanseList' => $allExpanseList]);

    }

    /**
     * Show Expense Report using DateRange Picker
     */
    function viewReport(Request $request)
    {
        $datePick = $request->input('datePicker');
        $dateSeparator = explode(" - ", $datePick);

        $s = strtotime($dateSeparator[0]);
        $e = strtotime($dateSeparator[1]);

        $start_date = date('Y-m-d', $s);
        $end_date = date('Y-m-d', $e);

        $allExpanseList = Expanse::whereBetween('date', [$start_date, $end_date])->latest()->get();

        return view('expanseReport', ['allExpanseList' => $allExpanseList]);

    }

    function newExpanse(Request $request)
    {
        $cat = Input::get('newCat');
        $newCat = new newCategory();
        $newCat->categoryName = $cat;


        if ($newCat->save()) {
            return json_encode(array('status' => true));
        }

    }

}
