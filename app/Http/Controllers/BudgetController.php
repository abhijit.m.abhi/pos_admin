<?php

namespace App\Http\Controllers;

use App\Budget;
use App\BudgetCategory;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\AddBudgetRequest;

class BudgetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allBudgetList = Budget::latest()->get();
        return view('budgetList', ['allBudgetList' => $allBudgetList]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categoryList = BudgetCategory::all();
        return view('addBudget', ['categoryList' => $categoryList]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(AddBudgetRequest $request)
    {
        $budget = new Budget();
        if (!empty($request->input('month_year'))) {
            $idate = $request->input('month_year');
            $start_date = date("Y-m-01", strtotime($idate));
            $end_date = date("Y-m-t", strtotime($idate));
        } else {
            $s_date = strtotime($request->input('start_date'));
            $start_date = date('Y-m-d', $s_date);
            $e_date = strtotime($request->input('end_date'));
            $end_date = date('Y-m-d', $e_date);
        }


        $budget->start_date = $start_date;

        $budget->end_date = $end_date;

        $budget->category = $request->input('budget_category');
        $budget->title = $request->input('title');
        $budget->amount = $request->input('amount');
        $budget->extend_amount = $request->input('extend_amount');
        $budget->comment = $request->input('comment');
        $budget->date = date('Y-m-d');
        $budget->saved_by = Auth::user()->id;


        if ($budget->save()) {
            //$request->session()->flash('status', 'success');
            Session::flash('message', 'Budget Added Successfully !');
        };

        return redirect('/budget');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $budgetData = Budget::find($id);
        $categoryList = BudgetCategory::all();
        return view('editBudget', ['budget' => $budgetData], ['categoryList' => $categoryList]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(AddBudgetRequest $request, Budget $budget)
    {
        $s_date = strtotime($request->input('start_date'));
        $start_date = date('Y-m-d', $s_date);
        $budget->start_date = $start_date;
        $e_date = strtotime($request->input('end_date'));
        $end_date = date('Y-m-d', $e_date);
        $budget->end_date = $end_date;

        $budget->category = $request->input('budget_category');
        $budget->title = $request->input('title');
        $budget->amount = $request->input('amount');
        $budget->extend_amount = $request->input('extend_amount');
        $budget->comment = $request->input('comment');


        if ($budget->update()) {
            Session::flash('message', 'Information Updated Successfully !');
            return redirect('/budget');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $budget = Budget::destroy($id);
    }

    function report()
    {

        $allBudgetList = Budget::orderby('date', 'desc')->get();
        return view('budgetReport', ['allBudgetList' => $allBudgetList]);

    }

    function viewReport(Request $request)
    {
        $datePick = $request->input('datePicker');
        $dateSeparator = explode(" - ", $datePick);

        $s = strtotime($dateSeparator[0]);
        $e = strtotime($dateSeparator[1]);

        $start_date = date('Y-m-d', $s);
        $end_date = date('Y-m-d', $e);

        $allBudgetList = Budget::whereBetween('date', [$start_date, $end_date])->orderby('date', 'desc')->get();

        return view('budgetReport', ['allBudgetList' => $allBudgetList]);

    }

    function newBudget(Request $request)
    {
        $cat = Input::get('newCat');
        $newCat = new BudgetCategory();
        $newCat->categoryName = $cat;


        if ($newCat->save()) {
            return json_encode(array('status' => true));
        }

    }

}
