<?php


namespace model;
namespace App\Http\Controllers;
use App\Installments;
use App\Loan;
use app\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;



class LoanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $loans = Loan::all();
        return view('loan.index',['loans' =>$loans]);
       //return "Hello it's working";

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('loan.addLoan');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //


        $loan=new Loan();

        $loan->account_no=$request->input('account_no');
        $loan->account_name=$request->input('account_name');
        $loan->bank_name=$request->input('bank_name');
        $loan->branch_name=$request->input('branch_name');
        $loan->bank_balance=$request->input('bank_balance');
        $loan->amount=$request->input('amount');
        $loan->percent=$request->input('percent');
        $loan_date = $request->input('loan_start_date');
        $start_date = strtotime($loan_date);
        $loan_start_date = date('Y-m-d', $start_date);
        $loan->loan_start_date=$loan_start_date;
        $year=$request->input('year');
        $month=$request->input('month');
        $date_=$request->input('date');
        $duration = array("$year", "$month", "$date_");
        $du = array_values($duration);

        $duration_=json_encode($du);
//
//        $x = json_decode($duration_,true);
//        var_dump($x) ;
//        exit;

        $loan->loan_duration=$duration_;
        $today = Carbon::today();
        $loan->date=$today;


//        echo $loan->loan_duration;
//        exit;

        $loan->comment=$request->input('comment');

        $loan->saved_by=Auth::user()->id;
        if($loan->save()){
            $request->session()->flash('message', 'Data added successfully!!!!!!!!!!');
        };
        return redirect('/loan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $loan = Loan::findOrFail($id);
        return view('loan.editLoan', compact('loan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,Loan $loan)
    {
        //
//        $loan = Loan::findOrFail($id);



        $loan->account_no=$request->input('account_no');
        $loan->account_name=$request->input('account_name');
        $loan->bank_name=$request->input('bank_name');
        $loan->branch_name=$request->input('branch_name');
        $loan->bank_balance=$request->input('bank_balance');
        $loan->amount=$request->input('amount');


        $loan->percent=$request->input('percent');

        $loan_date = $request->input('loan_start_date');
        $start_date = strtotime($loan_date);
        $loan_start_date = date('Y-m-d', $start_date);
        $loan->loan_start_date=$loan_start_date;


        $year=$request->input('year');
        $month=$request->input('month');
        $date_=$request->input('date');
        $duration = array("$year", "$month", "$date_");
        $du = array_values($duration);

        $duration_=json_encode($du);
        $loan->loan_duration=$duration_;
        $today = Carbon::today();
        $loan->date=$today;


//        echo $loan->loan_duration;
//        exit;

        $loan->comment=$request->input('comment');

//        $loan->saved_by=Auth::user()->id;



//        $loan->save();

        if ($loan->update()) {
            //$request->session()->flash('status', 'success');
           // Session::flash('message', 'Data Edited Successfully !');
            return redirect()->route('loan.index');
        };
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

        $loan = Loan::destroy($id);
       // return view('loan.index');

        //$loan->delete();
//        return redirect()->route('loan.index')->with('alert-success','Data Deleted');
    }
    public function deleteLoan($id){
        $loan = Loan::destroy($id);

        return response::json($loan);
    }
    public function report()
    {

        $allLoanList = Loan::orderby('date', 'desc')->get();
        return view('loan.report', ['allLoanList' => $allLoanList]);

    }

    public function showReport(Request $request)
    {

        $s_date = $request->input('start_date');
        $e_date = $request->input('end_date');

        $s = strtotime($s_date);
        $e = strtotime($e_date);

        $start_date = date('Y-m-d', $s);
        $end_date = date('Y-m-d', $e);



        $allLoanList = Loan::whereBetween('loan_start_date', [$start_date, $end_date])->orderby('loan_start_date', 'desc')->get();

        return view('loan.report', ['allLoanList' => $allLoanList]);

//        return view('');
    }

    public function matchAccount($accountNo)
    {
        //$account_no = Request::get('account');
        echo $accountNo;
        exit;
        $data = [
            "error" => "Name already used........"
        ];

        $Count = Loan::where('account_no', '=', Input::get('account_no'))->get()->count();
        if (count == 0)
        {
            return response::json($Count);
        }
        else{
            return response::json($Count,$data);
        }
    }


}
