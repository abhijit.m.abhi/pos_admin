<?php

namespace App\Http\Controllers;

use App\Expanse;
use App\Income;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class BalanceReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function openReport()
    {
        $allIncomeList = Income::latest()->get();
        $allExpanseList = Expanse::latest()->get();

        $incomeTotal = Income::sum('amount');
        $expanseTotal = Expanse::sum('amount');


        $totalBalance = $incomeTotal - $expanseTotal;


        return view('balanceReport', ['allIncomeList' => $allIncomeList, 'allExpanseList' => $allExpanseList, 'incomeTotal' => $incomeTotal, 'expanseTotal' => $expanseTotal, 'totalBalance' => $totalBalance]);
    }

    public function viewReport(Request $request)
    {
        $datePick = $request->input('datePicker');
        $dateSeparator = explode(" - ",$datePick);

        $s = strtotime($dateSeparator[0]);
        $e = strtotime($dateSeparator[1]);

        $start_date = date('Y-m-d', $s);
        $end_date = date('Y-m-d', $e);

        $allIncomeList = Income::whereBetween('date', [$start_date, $end_date])->latest()->get();
        $allExpanseList = Expanse::whereBetween('date', [$start_date, $end_date])->latest()->get();

        $incomeTotal = Income::whereBetween('date', [$start_date, $end_date])->sum('amount');
        $expanseTotal = Expanse::whereBetween('date', [$start_date, $end_date])->sum('amount');
        $totalBalance = $incomeTotal - $expanseTotal;


        return view('balanceReport', ['allIncomeList' => $allIncomeList, 'allExpanseList' => $allExpanseList, 'incomeTotal' => $incomeTotal, 'expanseTotal' => $expanseTotal, 'totalBalance' => $totalBalance, 's_date' => $dateSeparator[0], 'e_date' => $dateSeparator[1]]);

    }
}
