<?php

namespace App\Http\Controllers;
use App\Installments;
use App\Loan;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Response;
use Illuminate\Support\Facades\Input;




class InstallmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

//        $allAccountName = Loan::all();
//        return view('loan.viewInstallments',['accountList'=> $allAccountName]);

    }

    public function view()
    {

        $allAccountName = Loan::all();
        return view('loan.viewInstallments',['accountList'=> $allAccountName]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $allAccountName = Loan::all();
        return view('loan.addInstallments',['accountList'=> $allAccountName]);



    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $installment=new Installments();

        $installment->deposit_amount=$request->input('deposit_amount');
        $installment->interest=$request->input('interest');
        $installment->bank_branch=$request->input('bank_branch');
        $installment->deposited_by=$request->input('deposited_by');

        $loan_date = $request->input('date');
        $start_date = strtotime($loan_date);
        $loan_start_date = date('Y-m-d', $start_date);
        $installment->date=$loan_start_date;

        $installment->loan_id=$request->input('id');
        $installment->saved_by=Auth::user()->id;
        if($installment->save()){
            $request->session()->flash('message', 'Data added successfully!!!!!!!!!!');
            return redirect('/viewinstallment');

        };


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $installment = Installments::findOrFail($id);
        return view('loan.editInstallments', compact('installment'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Installments $installment)
    {
       $installment = Installments::findOrFail($id);
        $installment->deposited_by=$request->input('deposited_by');

        $installment->deposit_amount=$request->input('deposit_amount');
        $installment->bank_branch=$request->input('bank_branch');
        $installment->interest=$request->input('interest');



        $loan_date = $request->input('date');
        $start_date = strtotime($loan_date);
        $loan_start_date = date('Y-m-d', $start_date);
        $installment->date=$loan_start_date;


        if ($installment->update()) {
//            Session::flash('message', 'Information Updated Successfully !');
            return redirect('/viewinstallment');
        }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $installment = Installments::destroy($id);
    }

    public function viewDetails()
    {
        $filter = Input::get('id');
        $results = Loan::where('account_no','=',$filter)->get();
        $test = json_encode($results);
        return Response::json($test);
    }

    public function indexDetails()
    {

        $filter = Input::get('id');

        $results = Loan::where('account_no','=',$filter)->get();
        $test = json_encode($results);

        //var_dump($test);
        return Response::json($test);

    }
    public function details()
    {
        $tp = Input::get('id');
        $results1 = Installments::where('loan_id','=',$tp)->get();
        $test1 = json_encode($results1);

       return Response::json($test1);

    }

    public function report()
    {
            $loan = Loan::all();
            $data1 = array();

            foreach($loan as $tp)
            {

                $accountNo = $tp->account_no;
                $accountName =  $tp->account_name;
                $bankName = $tp->bank_name;
                $amount =  $tp->amount;
                $percent = $tp->percent;
                $id= $tp->id;
                $date = $tp->loan_start_date;

                $in_stock = DB::table('loan_installments')
                    ->where('loan_id', '=', $id)
                    ->get();
                $totalInterest = $in_stock->sum('interest');
                $totalAmount = $in_stock->sum('deposit_amount');
                $data = array ("id"=>$id,"accNo"=>$accountNo,"accName"=>$accountName,"bankName"=>$bankName,"amount"=>$amount,"percent"=>$percent,"totalInterest"=>$totalInterest,"totalAmount"=>$totalAmount,"date"=>$date);

                array_push($data1, $data);
            }

        return view('loan.reportInstallments', compact('data1'));
    }

   public function showReportDetails(Request $request)
   {

       $s_date = $request->input('start_date');
       $e_date = $request->input('end_date');

       $s = strtotime($s_date);
       $e = strtotime($e_date);

       $start_date = date('Y-m-d', $s);
       $end_date = date('Y-m-d', $e);

       $loan = Loan::whereBetween('loan_start_date', [$start_date, $end_date])->get();


       $data1 = array();

       foreach ($loan as $tp) {

           $accountNo = $tp->account_no;
           $accountName = $tp->account_name;
           $bankName = $tp->bank_name;
           $amount = $tp->amount;
           $percent = $tp->percent;
           $id = $tp->id;
           $date = $tp->loan_start_date;

           $in_stock = DB::table('loan_installments')
               ->where('loan_id', '=', $id)
               ->get();

           $totalInterest = $in_stock->sum('interest');
           $totalAmount = $in_stock->sum('deposit_amount');
           $data = array ("id"=>$id,"accNo"=>$accountNo,"accName"=>$accountName,"bankName"=>$bankName,"amount"=>$amount,"percent"=>$percent,"totalInterest"=>$totalInterest,"totalAmount"=>$totalAmount,"date"=>$date);

           array_push($data1, $data);
       }

       return view('loan.reportInstallments', compact('data1'));










      /* $loan = DB::table('tbl_loan')
           ->join('loan_installments', 'tbl_loan.id', '=', 'loan_installments.loan_id')
           ->whereBetween('date', [$start_date, $end_date])->orderby('loan_start_date', 'desc')
           ->get();*/

      // $allLoanList = Loan::whereBetween('loan_start_date', [$start_date, $end_date])->orderby('loan_start_date', 'desc')->get();

       //return view('loan.reportInstallments', ['allLoanList' => $loan]);

   }

}
