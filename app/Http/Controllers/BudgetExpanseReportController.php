<?php

namespace App\Http\Controllers;

use App\Budget;
use App\Expanse;
use Illuminate\Http\Request;

class BudgetExpanseReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function openReport()
    {
        $allBudgetList = Budget::latest()->get();
        $allExpanseList = Expanse::latest()->get();
        $budgetTotal = Budget::sum('amount');
        $budgetExtendTotal = Budget::sum('extend_amount');
        $expanseTotal = Expanse::sum('amount');
        $totalBalance = ($budgetTotal + $budgetExtendTotal) - $expanseTotal;
        return view('budgetExpanseReport', ['allBudgetList' => $allBudgetList, 'allExpanseList' => $allExpanseList, 'budgetTotal' => $budgetTotal, 'budgetExtendTotal' => $budgetExtendTotal,'expanseTotal' => $expanseTotal, 'totalBalance' => $totalBalance]);
    }
    public function viewReport(Request $request)
    {
        $datePick = $request->input('datePicker');
        $dateSeparator = explode(" - ",$datePick);

        $s = strtotime($dateSeparator[0]);
        $e = strtotime($dateSeparator[1]);

        $start_date = date('Y-m-d', $s);
        $end_date = date('Y-m-d', $e);

        $allBudgetList = Budget::whereBetween('date', [$start_date, $end_date])->latest()->get();
        $allExpanseList = Expanse::whereBetween('date', [$start_date, $end_date])->latest()->get();

        $budgetTotal = Budget::whereBetween('date', [$start_date, $end_date])->sum('amount');
        $budgetExtendTotal = Budget::whereBetween('date', [$start_date, $end_date])->sum('extend_amount');
        $expanseTotal = Expanse::whereBetween('date', [$start_date, $end_date])->sum('amount');
        $totalBalance = ($budgetTotal + $budgetExtendTotal) - $expanseTotal;


        return view('budgetExpanseReport', ['allBudgetList' => $allBudgetList, 'allExpanseList' => $allExpanseList, 'budgetTotal' => $budgetTotal, 'budgetExtendTotal' => $budgetExtendTotal, 'expanseTotal' => $expanseTotal, 'totalBalance' => $totalBalance, 's_date' => $dateSeparator[0], 'e_date' => $dateSeparator[1]]);

    }
}
