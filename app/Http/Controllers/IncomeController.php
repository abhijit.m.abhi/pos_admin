<?php

namespace App\Http\Controllers;

use App\Http\Requests\AddIncomeRequest;
use App\Http\Requests\EditIncomeRequest;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Income;

class IncomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allIncomeList = Income::latest()->get();
        //dd($allIncomeList);
        return view('incomeList', ['allIncomeList' => $allIncomeList]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('addIncome');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(AddIncomeRequest $request)
    {
        $income = new Income();

        $income_date = $request->input('date');
        $income_date_string = strtotime($income_date);
        $income_date_formatted = date('Y-m-d', $income_date_string);
        $income->date = $income_date_formatted;

        $income->head = $request->input('type');
        $income->amount = $request->input('amount');
        $income->saved_by = Auth::user()->id;


        if ($income->save()) {
            //$request->session()->flash('status', 'success');
            Session::flash('message', 'Income Added Successfully !');
        };

        return redirect('/income');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $incomeData = Income::findOrFail($id);
        return view('editIncome', ['income' => $incomeData]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(EditIncomeRequest $request, Income $income)
    {
        $income_date = $request->input('date');
        $income_date_string = strtotime($income_date);
        $income_date_formatted = date('Y-m-d', $income_date_string);
        $income->date = $income_date_formatted;

        $income->head = $request->input('type');
        $income->amount = $request->input('amount');
        $income->saved_by = Auth::user()->id;


        if ($income->update()) {
            //$request->session()->flash('status', 'success');
            Session::flash('message', 'Information Updated Successfully !');
            return redirect('/income');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $income = Income::destroy($id);
    }


    function report()
    {

        $allIncomeList = Income::latest()->get();
        return view('incomeReport', ['allIncomeList' => $allIncomeList]);

    }


    function viewReport(Request $request)
    {
        $datePick = $request->input('datePicker');
        $dateSeparator = explode(" - ", $datePick);

        $s = strtotime($dateSeparator[0]);
        $e = strtotime($dateSeparator[1]);

        $start_date = date('Y-m-d', $s);
        $end_date = date('Y-m-d', $e);

        $allIncomeList = Income::whereBetween('date', [$start_date, $end_date])->latest()->get();

        return view('incomeReport', ['allIncomeList' => $allIncomeList]);

    }
}
