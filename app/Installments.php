<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Installments extends Model
{
    //
    protected $table = 'loan_installments';
    protected $fillable = ['deposit_amount',
        'interest',
        'bank_branch',
        'deposited_by' ,
        'loan_id',
        'date',
        'saved_by'
    ];


    public function loan_id(){
        return $this->belongsTo('App\Loan');
    }
}

