<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\Income;
use App\newCategory;
use App\Loan;
use App\BudgetCategory;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\DB;

Route::get('login', 'Auth\LoginController@showLoginForm');
Route::post('login', 'Auth\LoginController@login');
Route::get('logout', 'Auth\LoginController@logout');

Route::group(['middleware' => 'auth'], function () {

    Route::get('/', function () {
        return view('dashboard');
    });

    Route::get('/home', 'HomeController@index');


    Route::get('/dashboard', function () {
        return view('dashboard');
    });

    Route::get('/table', function () {
        return view('tables');
    });

    Route::get('/table_dynamic', function () {
        return view('tables_dynamic');
    });

    /**
     * Profile Route
     */

    Route::get('profile', 'UserController@profile');

    /**
     *  Expense Route
     */


    Route::post('/expanse/appendCat',function(){

        $categoryList = newCategory::orderBy('created_at', 'desc')->first();
        return response::json($categoryList);
    });

    Route::get('expanse/report', 'ExpanseController@report');
    Route::post('expanse/viewReport', 'ExpanseController@viewReport');
    Route::any('expanse/add-new-expense', 'ExpanseController@newExpanse');
    Route::resource('expanse', 'ExpanseController');

    Route::delete('/expanse/{id?}',function($id){

        $expanse = Expanse::destroy($id);
        return response::json($expanse);
    });

    /*
     * User Route
     */

    Route::resource('user', 'UserController');

    Route::delete('/user/{id?}',function($id){

        $user = User::destroy($id);
        return response::json($user);
    });



    /**
     * Delete Route
     */

    Route::get('/delete-expanse/{id}', 'ExpanseController@delete_expanse');
    Route::get('/delete-user/{id}', 'UserController@delete_user');

    /**
     * Loan Route
     */
    Route::get('loan/report', 'LoanController@report');
    Route::post('loan/view-report', 'LoanController@showReport');
    Route::resource('loan', 'LoanController');

    Route::delete('/loan/{id?}',function($id){
        $loan = Loan::destroy($id);
        return response::json($loan);
    });

    /**
     * Budget Route
     */
    Route::post('/budget/append-new-cat',function(){

        $categoryList = BudgetCategory::orderBy('created_at', 'desc')->first();
        return response::json($categoryList);
    });

    Route::get('budget/report', 'BudgetController@report');
    Route::post('budget/viewReport', 'BudgetController@viewReport');
    Route::any('budget/add-new-budget', 'BudgetController@newBudget');
    Route::resource('budget', 'BudgetController');

//    Route::delete('/budget/{id?}',function($id){
//
//        $expanse = Expanse::destroy($id);
//        return response::json($expanse);
//    });


    /**
     * Income Route
     */

    Route::get('income/report', 'IncomeController@report');
    Route::post('income/viewReport', 'IncomeController@viewReport');
    Route::resource('income', 'IncomeController');

    Route::delete('/income/{id?}',function($id){

        $income = Income::destroy($id);
        return response::json($income);
    });
   // Route::post('/loan/check/{accountNo}', 'LoanController@matchAccount');


    Route::get('check/{accountNo?}',function(){
        $account_no = $_GET['account'];
     //  $check = Loan::matchAccount($account_no);
        $c = Loan::where('account_no', '=', $account_no)->get()->count();
        if ($c == 0)
        {
           echo $a = "ok";
            //return response::json($a);

        }
        else{

            echo   $a = "not";
          }

    });

    /* Loan Installment */

    //Route::get('create/installments', 'InstallmentController@showInstallmentsReport');
    Route::resource('installments', 'InstallmentController');
    Route::get('addinstallment', 'InstallmentController@viewDetails');
    Route::get('viewinstallment', 'InstallmentController@view');

    Route::get('viewinstallment/report', 'InstallmentController@report');
    Route::post('viewinstallment/viewreport', 'InstallmentController@showReportDetails');

    Route::delete('/delete/{id?}',function($id){
        $installment = \App\Installments::destroy($id);
        return response::json($installment);
    });



    Route::get('report/openReport', 'BalanceReportController@openReport');
    Route::post('report/viewReport', 'BalanceReportController@viewReport');

    Route::get('report/budget-expanse', 'BudgetExpanseReportController@openReport');
    Route::post('report/view-budget-expanse', 'BudgetExpanseReportController@viewReport');
    Route::get('add_installment', 'InstallmentController@indexDetails');
    Route::get('add_details', 'InstallmentController@details');

   // Route::post('delete', 'InstallmentController@delete_installment');





});


