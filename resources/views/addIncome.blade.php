@extends('layouts.master')
@section('title', 'Add Income')
@section('content')


    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Income</h3>
                </div>

            </div>
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Add Income<!--<small>Input different form elements</small>--></h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">

                            <form action="{{url('income')}}" method="POST" class="form-horizontal" role="form">
                                {{ csrf_field() }}
                                <div class="form-body">

                                    <div class="form-group" id="type" style='display:block;'>
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Date</label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input name="date" class="birthday date-picker form-control col-md-7 col-xs-12" required="required" type="text">
                                        </div>
                                    </div>

                                    <div class="form-group" id="type" style='display:block;'>
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Type</label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="text" class="form-control col-md-7 col-xs-12" name="type"
                                                   placeholder="Type">
                                            @if ($errors->has('type'))
                                                <span class="help-block">
                                                <strong>{{ $errors->first('type') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group" style='display:block;'>
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Amount</label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="number" class="form-control col-md-7 col-xs-12" name="amount"
                                                   placeholder="Amount">
                                            @if ($errors->has('amount'))
                                                <span class="help-block">
                                                <strong>{{ $errors->first('amount') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="ln_solid"></div>
                                    <div class="form-group">
                                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                            <a type="cancel" href="{{ url('dashboard') }}"
                                               class="btn btn-danger">Cancel</a>
                                            <button type="submit" class="btn btn-primary">Add</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection