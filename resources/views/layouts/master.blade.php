<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>POS Admin | @yield('title') </title>
    <link rel="shortcut icon" href="{!! asset('/assets/template/favicon_cart02.ico') !!}">
    <!-- Data Table Total Amount -->

    {{--<script src="http://code.jquery.com/jquery-1.12.4.js"></script>--}}
    {{--<script src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>--}}
    <script src="{{ asset('/assets/template/vendors/dataTableJS/jquery-1.12.4.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/assets/template/vendors/dataTableJS/jquery.dataTables.min.js') }}"
            type="text/javascript"></script>

    <!-- Data Table Total Amount JS -->
    <script src="{{ asset('/assets/template/vendors/totalAmount/tamount.js') }}" type="text/javascript"></script>

    <!-- Bootstrap -->
    <link href="{{ asset('/assets/template/vendors/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet"
          type="text/css"/>
    <!-- Font Awesome -->
    <link href="{{ asset('/assets/template/vendors/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet"
          type="text/css"/>
    <!-- NProgress -->
    <link href="{{ asset('/assets/template/vendors/nprogress/nprogress.css') }}" rel="stylesheet" type="text/css"/>
    <!-- iCheck -->
    <link href="{{ asset('/assets/template/vendors/iCheck/skins/flat/green.css') }}" rel="stylesheet" type="text/css"/>
    <!-- Datatables -->
    <link href="{{ asset('/assets/template/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css') }}"
          rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/assets/template/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css') }}"
          rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/assets/template/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css') }}"
          rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/assets/template/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css') }}"
          rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/assets/template/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css') }}"
          rel="stylesheet" type="text/css"/>


    <!-- bootstrap-wysiwyg -->
    <link href="{{ asset('/assets/template/vendors/google-code-prettify/bin/prettify.min.css') }}" rel="stylesheet"
          type="text/css"/>
    <!-- Select2 -->
    <link href="{{ asset('/assets/template/vendors/select2/dist/css/select2.min.css') }}" rel="stylesheet"
          type="text/css"/>
    <!-- Switchery -->
    <link href="{{ asset('/assets/template/vendors/switchery/dist/switchery.min.css') }}" rel="stylesheet"
          type="text/css"/>
    <!-- starrr -->
    <link href="{{ asset('/assets/template/vendors/starrr/dist/starrr.css') }}" rel="stylesheet" type="text/css"/>
    <!-- bootstrap-progressbar -->
    <link href="{{ asset('/assets/template/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css') }}"
          rel="stylesheet" type="text/css"/>
    <!-- JQVMap -->
    <link href="{{ asset('/assets/template/vendors/jqvmap/dist/jqvmap.min.css') }}" rel="stylesheet" type="text/css"/>
    <!-- bootstrap-daterangepicker -->
    <link href="{{ asset('/assets/template/vendors/bootstrap-daterangepicker/daterangepicker.css') }}" rel="stylesheet"
          type="text/css"/>
    <!-- Custom Theme Style -->
    {{--<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css">--}}
    {{--<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.2.4/css/buttons.dataTables.min.css">--}}

    <link href="{{ asset('/assets/template/build/css/custom.min.css') }}" rel="stylesheet" type="text/css"/>
    <!-- month view date picker -->
    <link href="http://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css"
          rel="stylesheet">

    <!-- data table css -->
    <Style>
        th {
            white-space: nowrap;
        }
        input[type=number] {-moz-appearance: textfield;}
        ::-webkit-inner-spin-button { -webkit-appearance: none;}
        ::-webkit-outer-spin-button { -webkit-appearance: none;}

    </Style>


</head>

<body class="nav-md">
<div class="container body">
    <div class="main_container">
        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">
                <div class="navbar nav_title" style="border: 0;">
                    <a href="{{ url('dashboard') }}" class="site_title"><i class="fa fa-paw"></i>
                        <span>Synergyforce!</span></a>
                </div>

                <div class="clearfix"></div>

                <!-- menu profile quick info -->
                <div class="profile clearfix">
                    <div class="profile_pic">
                        <!--<img src="images/img.jpg" alt="..." class="img-circle profile_img">-->
                        <img src="{{ asset('/images/users/'.Auth::user()->photo) }}" alt=" "
                             class="img-circle profile_img">
                    </div>
                    <div class="profile_info">
                        <span>Welcome,</span>
                        <h2>{{Auth::user()->fname." ".Auth::user()->lname}}</h2>
                    </div>
                </div>


                <br/>

                <!-- sidebar menu -->
                <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                    <div class="menu_section">
                        <h3>General</h3>
                        <ul class="nav side-menu">
                            <li><a href="{{ url('dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard </a></li>
                            <li><a href="{{ url('#') }}"><i class="fa fa-cogs"></i> POS </a></li>
                            <li><a href="{{ url('#') }}"><i class="fa fa-cogs"></i> Accounts </a></li>
                            <li><a><i class="fa fa-leaf"></i> Budget <span class="fa fa-chevron-down"></span>
                                </a>
                                <ul class="nav child_menu">
                                    <li><a href="{{ url('budget/create') }}">Add Budget</a></li>
                                    <li><a href="{{ url('/budget') }}">View Budget</a></li>
                                    <li><a href="{{ url('budget/report') }}">Budget Report</a></li>
                                </ul>
                            </li>
                            <li><a><i class="fa fa-calculator"></i> Expense <span class="fa fa-chevron-down"></span>
                                </a>
                                <ul class="nav child_menu">
                                    <li><a href="{{ url('expanse/create') }}">Add Expense</a></li>
                                    <li><a href="{{ url('/expanse') }}">View Expense</a></li>
                                    <li><a href="{{ url('expanse/report') }}">Expense Report</a></li>
                                </ul>
                            </li>
                            <li><a><i class="fa fa-briefcase"></i> Income <span class="fa fa-chevron-down"></span>
                                </a>
                                <ul class="nav child_menu">
                                    <li><a href="{{ url('income/create') }}">Add Income</a></li>
                                    <li><a href="{{ url('/income') }}">View Income</a></li>
                                    <li><a href="{{ url('income/report') }}">Income Report</a></li>
                                </ul>
                            </li>
                            <li><a><i class="fa fa-fire"></i> Loan <span class="fa fa-chevron-down"></span> </a>
                                <ul class="nav child_menu">
                                    <li><a href="{{ url('loan/create') }}">Add Loan</a></li>
                                    <li><a href="{{ url('/loan') }}">View Loan</a></li>
                                    <li><a href="{{ url('loan/report') }}">Report</a></li>
                                </ul>
                            </li>
                            <li><a><i class="fa fa-fire"></i> Loan Installments <span class="fa fa-chevron-down"></span>
                                </a>
                                <ul class="nav child_menu">
                                    <li><a href="{{ url('installments/create') }}">Add Installments</a></li>
                                    <li><a href="{{ url('/viewinstallment') }}">View Installments</a></li>
                                    <li><a href="{{ url('viewinstallment/report') }}">Report</a></li>
                                </ul>
                            </li>
                            <li><a><i class="fa fa-newspaper-o"></i> Report <span class="fa fa-chevron-down"></span>
                                </a>
                                <ul class="nav child_menu">
                                    <li><a href="{{ url('report/openReport') }}">Balance Report</a></li>
                                    <li><a href="{{ url('report/budget-expanse') }}">Budget Expense Report</a></li>
                                </ul>
                            </li>
                            <li><a href="{{ url('#') }}"><i class="fa fa-cogs"></i> Settings </a></li>
                            <li><a><i class="fa fa-user"></i> User <span class="fa fa-chevron-down"></span> </a>
                                <ul class="nav child_menu">
                                    <li><a href="{{ url('user/create') }}">Add User</a></li>
                                    <li><a href="{{ url('/user') }}">View User</a></li>
                                </ul>
                            </li>
                            </li>
                        </ul>
                    </div>

                </div>
                <!-- /sidebar menu -->

                <!-- /menu footer buttons -->
                <div class="sidebar-footer hidden-small">
                    <a data-toggle="tooltip" data-placement="top" title="Settings">
                        <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                        <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="Lock">
                        <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
                    </a>
                    <a href="{{URL('logout')}}" data-toggle="tooltip" data-placement="top" title="Logout">
                        <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                    </a>
                </div>
                <!-- /menu footer buttons -->
            </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
            <div class="nav_menu">
                <nav>
                    <div class="nav toggle">
                        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                    </div>

                    <ul class="nav navbar-nav navbar-right">
                        <li class="">
                            <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown"
                               aria-expanded="false">
                                <img src="{{ asset('/images/users/'.Auth::user()->photo) }}"
                                     alt="">{{Auth::user()->fname." ".Auth::user()->lname}}
                                <span class=" fa fa-angle-down"></span>
                            </a>
                            <ul class="dropdown-menu dropdown-usermenu pull-right">
                                <li><a href="{{ url('profile') }}"> Profile</a></li>
                                <li>
                                    <a href="javascript:;">
                                        <span class="badge bg-red pull-right">50%</span>
                                        <span>Settings</span>
                                    </a>
                                </li>
                                <li><a href="javascript:;">Help</a></li>
                                <li><a href="{{URL('logout')}}"><i class="fa fa-sign-out pull-right"></i> Log Out</a>
                                </li>
                            </ul>
                        </li>

                        <li role="presentation" class="dropdown">
                            <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown"
                               aria-expanded="false">
                                <i class="fa fa-envelope-o"></i>
                                <span class="badge bg-green">6</span>
                            </a>
                            <ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
                                <li>
                                    <a>
                                        <span class="image"><img
                                                    src="{{ asset('/assets/template/production/images/img.jpg') }}"
                                                    alt="Profile Image"/></span>
                                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                                    </a>
                                </li>
                                <li>
                                    <a>
                                        <span class="image"><img
                                                    src="{{ asset('/assets/template/production/images/img.jpg') }}"
                                                    alt="Profile Image"/></span>
                                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                                    </a>
                                </li>
                                <li>
                                    <a>
                                        <span class="image"><img
                                                    src="{{ asset('/assets/template/production/images/img.jpg') }}"
                                                    alt="Profile Image"/></span>
                                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                                    </a>
                                </li>
                                <li>
                                    <a>
                                        <span class="image"><img
                                                    src="{{ asset('/assets/template/production/images/img.jpg') }}"
                                                    alt="Profile Image"/></span>
                                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                                    </a>
                                </li>
                                <li>
                                    <div class="text-center">
                                        <a>
                                            <strong>See All Alerts</strong>
                                            <i class="fa fa-angle-right"></i>
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->

    @yield('content')

    <!-- /page content -->

        <!-- footer content -->
        <footer>
            <div class="pull-right">
                Powered by <a href="http://synergyforcesolutions.com/" target='_blank'>Synergyforce Solutions</a>
            </div>
            <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
    </div>
</div>

<!-- jQuery -->
<script src="{{ asset('/assets/template/vendors/jquery/dist/jquery.min.js') }}" type="text/javascript"></script>


<script src="{{ asset('/assets/template/vendors/jquery/dist/jquery.confirm.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/template/vendors/jquery/dist/jquery.confirm.min.js') }}" type="text/javascript"></script>

{{--<script src="{{ asset('/assets/template/vendors/jquery/dist/TotalAmount/code.jquery.com/jquery-1.12.4.js') }}" type="text/javascript"></script>--}}
{{--<script src="{{ asset('/assets/template/vendors/jquery/dist/TotalAmount/jquery.dataTables.min.js') }}" type="text/javascript"></script>--}}
{{----}}

<!-- Bootstrap -->
<script src="{{ asset('/assets/template/vendors/bootstrap/dist/js/bootstrap.min.js') }}"
        type="text/javascript"></script>
<!-- FastClick -->
<script src="{{ asset('/assets/template/vendors/fastclick/lib/fastclick.js') }}" type="text/javascript"></script>
<!-- NProgress -->
<script src="{{ asset('/assets/template/vendors/nprogress/nprogress.js') }}" type="text/javascript"></script>
<!-- Chart.js -->
<script src="{{ asset('/assets/template/vendors/Chart.js/dist/Chart.min.js') }}" type="text/javascript"></script>
<!-- gauge.js -->
<script src="{{ asset('/assets/template/vendors/gauge.js/dist/gauge.min.js') }}" type="text/javascript"></script>
<!-- bootstrap-progressbar -->
<script src="{{ asset('/assets/template/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js') }}"
        type="text/javascript"></script>
<!-- iCheck -->

<script src="{{ asset('/assets/template/vendors/iCheck/icheck.min.js') }}" type="text/javascript"></script>

<!-- Datatables -->
<script src="{{ asset('/assets/template/vendors/datatables.net/js/jquery.dataTables.min.js') }}"
        type="text/javascript"></script>
<script src="{{ asset('/assets/template/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"
        type="text/javascript"></script>
<script src="{{ asset('/assets/template/vendors/datatables.net-buttons/js/dataTables.buttons.min.js') }}"
        type="text/javascript"></script>
<script src="{{ asset('/assets/template/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js') }}"
        type="text/javascript"></script>
<script src="{{ asset('/assets/template/vendors/datatables.net-buttons/js/buttons.flash.min.js') }}"
        type="text/javascript"></script>
<script src="{{ asset('/assets/template/vendors/datatables.net-buttons/js/buttons.html5.min.js') }}"
        type="text/javascript"></script>
<script src="{{ asset('/assets/template/vendors/datatables.net-buttons/js/buttons.print.min.js') }}"
        type="text/javascript"></script>
<script src="{{ asset('/assets/template/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js') }}"
        type="text/javascript"></script>
<script src="{{ asset('/assets/template/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js') }}"
        type="text/javascript"></script>
<script src="{{ asset('/assets/template/vendors/datatables.net-responsive/js/dataTables.responsive.min.js') }}"
        type="text/javascript"></script>
<script src="{{ asset('/assets/template/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js') }}"
        type="text/javascript"></script>
<script src="{{ asset('/assets/template/vendors/datatables.net-scroller/js/datatables.scroller.min.js') }}"
        type="text/javascript"></script>
<script src="{{ asset('/assets/template/vendors/pdfmake/build/pdfmake.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/template/vendors/pdfmake/build/vfs_fonts.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/template/vendors/jszip/dist/jszip.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/template/vendors/jquery/dist/buttons.colVis.min.js') }}" type="text/javascript"></script>


<!-- Skycons -->
<script src="{{ asset('/assets/template/vendors/skycons/skycons.js') }}" type="text/javascript"></script>
<!-- Flot -->
<script src="{{ asset('/assets/template/vendors/Flot/jquery.flot.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/template/vendors/Flot/jquery.flot.pie.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/template/vendors/Flot/jquery.flot.time.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/template/vendors/Flot/jquery.flot.stack.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/template/vendors/Flot/jquery.flot.resize.js') }}" type="text/javascript"></script>
<!-- Flot plugins -->
<script src="{{ asset('/assets/template/vendors/flot.orderbars/js/jquery.flot.orderBars.js') }}"
        type="text/javascript"></script>
<script src="{{ asset('/assets/template/vendors/flot-spline/js/jquery.flot.spline.min.js') }}"
        type="text/javascript"></script>
<script src="{{ asset('/assets/template/vendors/flot.curvedlines/curvedLines.js') }}" type="text/javascript"></script>
<!-- DateJS -->
<script src="{{ asset('/assets/template/vendors/DateJS/build/date.js') }}" type="text/javascript"></script>
<!-- JQVMap -->
<script src="{{ asset('/assets/template/vendors/jqvmap/dist/jquery.vmap.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/template/vendors/jqvmap/dist/maps/jquery.vmap.world.js') }}"
        type="text/javascript"></script>
<script src="{{ asset('/assets/template/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js') }}"
        type="text/javascript"></script>

<!-- Custom Theme Scripts -->
<script src="{{ asset('/assets/template/build/js/custom.min.js') }}" type="text/javascript"></script>


<!-- bootstrap-daterangepicker -->
<script src="{{ asset('/assets/template/vendors/moment/min/moment.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/template/vendors/bootstrap-daterangepicker/daterangepicker.js') }}"
        type="text/javascript"></script>


<!-- bootstrap-wysiwyg -->
<script src="{{ asset('/assets/template/vendors/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js') }}"
        type="text/javascript"></script>
<script src="{{ asset('/assets/template/vendors/jquery.hotkeys/jquery.hotkeys.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/template/vendors/google-code-prettify/src/prettify.js') }}"
        type="text/javascript"></script>


<!-- jQuery Tags Input -->
<script src="{{ asset('/assets/template/vendors/jquery.tagsinput/src/jquery.tagsinput.js') }}"
        type="text/javascript"></script>

<!-- Switchery -->
<script src="{{ asset('/assets/template/vendors/switchery/dist/switchery.min.js') }}" type="text/javascript"></script>

<!-- Select2 -->
<script src="{{ asset('/assets/template/vendors/select2/dist/js/select2.full.min.js') }}"
        type="text/javascript"></script>

<!-- Parsley -->
<script src="{{ asset('/assets/template/vendors/parsleyjs/dist/parsley.min.js') }}" type="text/javascript"></script>

<!-- Autosize -->
<script src="{{ asset('/assets/template/vendors/autosize/dist/autosize.min.js') }}" type="text/javascript"></script>

<!-- jQuery autocomplete -->
<script src="{{ asset('/assets/template/vendors/devbridge-autocomplete/dist/jquery.autocomplete.min.js') }}"
        type="text/javascript"></script>

<!-- starrr -->
<script src="{{ asset('/assets/template/vendors/starrr/dist/starrr.js') }}" type="text/javascript"></script>
{{--<script src="{{ asset('/assets/template/build/js/custom.min.js') }}" type="text/javascript"></script>--}}
{{--Loan Validation--}}


<!-- month view date picker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js"></script>
<script>
    $(document).ready(function () {
        $('.birthday').daterangepicker({
            singleDatePicker: true,
            calender_style: "picker_4"
        }, function (start, end, label) {
            console.log(start.toISOString(), end.toISOString(), label);
        });
    });
</script>

<!-- /bootstrap-daterangepicker -->

<!-- Datatables -->


<!-- Flot -->
<script>
    $(document).ready(function () {
        var data1 = [
            [gd(2012, 1, 1), 17],
            [gd(2012, 1, 2), 74],
            [gd(2012, 1, 3), 6],
            [gd(2012, 1, 4), 39],
            [gd(2012, 1, 5), 20],
            [gd(2012, 1, 6), 85],
            [gd(2012, 1, 7), 7]
        ];

        var data2 = [
            [gd(2012, 1, 1), 82],
            [gd(2012, 1, 2), 23],
            [gd(2012, 1, 3), 66],
            [gd(2012, 1, 4), 9],
            [gd(2012, 1, 5), 119],
            [gd(2012, 1, 6), 6],
            [gd(2012, 1, 7), 9]
        ];
        $("#canvas_dahs").length && $.plot($("#canvas_dahs"), [
            data1, data2
        ], {
            series: {
                lines: {
                    show: false,
                    fill: true
                },
                splines: {
                    show: true,
                    tension: 0.4,
                    lineWidth: 1,
                    fill: 0.4
                },
                points: {
                    radius: 0,
                    show: true
                },
                shadowSize: 2
            },
            grid: {
                verticalLines: true,
                hoverable: true,
                clickable: true,
                tickColor: "#d5d5d5",
                borderWidth: 1,
                color: '#fff'
            },
            colors: ["rgba(38, 185, 154, 0.38)", "rgba(3, 88, 106, 0.38)"],
            xaxis: {
                tickColor: "rgba(51, 51, 51, 0.06)",
                mode: "time",
                tickSize: [1, "day"],
                //tickLength: 10,
                axisLabel: "Date",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 10
            },
            yaxis: {
                ticks: 8,
                tickColor: "rgba(51, 51, 51, 0.06)",
            },
            tooltip: false
        });

        function gd(year, month, day) {
            return new Date(year, month - 1, day).getTime();
        }
    });
</script>
<!-- /Flot -->

<!-- JQVMap -->
<script>
    $(document).ready(function () {
        $('#world-map-gdp').vectorMap({
            map: 'world_en',
            backgroundColor: null,
            color: '#ffffff',
            hoverOpacity: 0.7,
            selectedColor: '#666666',
            enableZoom: true,
            showTooltip: true,
            values: sample_data,
            scaleColors: ['#E6F2F0', '#149B7E'],
            normalizeFunction: 'polynomial'
        });
    });
</script>
<!-- /JQVMap -->

<!-- Skycons -->
<script>
    $(document).ready(function () {
        var icons = new Skycons({
                "color": "#73879C"
            }),
            list = [
                "clear-day", "clear-night", "partly-cloudy-day",
                "partly-cloudy-night", "cloudy", "rain", "sleet", "snow", "wind",
                "fog"
            ],
            i;

        for (i = list.length; i--;)
            icons.set(list[i], list[i]);

        icons.play();
    });
</script>
<!-- /Skycons -->

<!-- Doughnut Chart -->
<script>
    $(document).ready(function () {
        var options = {
            legend: false,
            responsive: false
        };

        new Chart(document.getElementById("canvas1"), {
            type: 'doughnut',
            tooltipFillColor: "rgba(51, 51, 51, 0.55)",
            data: {
                labels: [
                    "Symbian",
                    "Blackberry",
                    "Other",
                    "Android",
                    "IOS"
                ],
                datasets: [{
                    data: [15, 20, 30, 10, 30],
                    backgroundColor: [
                        "#BDC3C7",
                        "#9B59B6",
                        "#E74C3C",
                        "#26B99A",
                        "#3498DB"
                    ],
                    hoverBackgroundColor: [
                        "#CFD4D8",
                        "#B370CF",
                        "#E95E4F",
                        "#36CAAB",
                        "#49A9EA"
                    ]
                }]
            },
            options: options
        });
    });
</script>
<!-- /Doughnut Chart -->


<!-- bootstrap-daterangepicker -->
<script>
    $(document).ready(function () {

        var cb = function (start, end, label) {
            console.log(start.toISOString(), end.toISOString(), label);
            $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        };

        var optionSet1 = {
            startDate: moment().subtract(29, 'days'),
            endDate: moment(),
            minDate: '01/01/2012',
            maxDate: '12/31/2015',
            dateLimit: {
                days: 60
            },
            showDropdowns: true,
            showWeekNumbers: true,
            timePicker: false,
            timePickerIncrement: 1,
            timePicker12Hour: true,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            opens: 'left',
            buttonClasses: ['btn btn-default'],
            applyClass: 'btn-small btn-primary',
            cancelClass: 'btn-small',
            format: 'MM/DD/YYYY',
            separator: ' to ',
            locale: {
                applyLabel: 'Submit',
                cancelLabel: 'Clear',
                fromLabel: 'From',
                toLabel: 'To',
                customRangeLabel: 'Custom',
                daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                firstDay: 1
            }
        };
        $('#reportrange span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));
        $('#reportrange').daterangepicker(optionSet1, cb);
        $('#reportrange').on('show.daterangepicker', function () {
            console.log("show event fired");
        });
        $('#reportrange').on('hide.daterangepicker', function () {
            console.log("hide event fired");
        });
        $('#reportrange').on('apply.daterangepicker', function (ev, picker) {
            console.log("apply event fired, start/end dates are " + picker.startDate.format('MMMM D, YYYY') + " to " + picker.endDate.format('MMMM D, YYYY'));
        });
        $('#reportrange').on('cancel.daterangepicker', function (ev, picker) {
            console.log("cancel event fired");
        });
        $('#options1').click(function () {
            $('#reportrange').data('daterangepicker').setOptions(optionSet1, cb);
        });
        $('#options2').click(function () {
            $('#reportrange').data('daterangepicker').setOptions(optionSet2, cb);
        });
        $('#destroy').click(function () {
            $('#reportrange').data('daterangepicker').remove();
        });
    });
</script>
<!-- /bootstrap-daterangepicker -->

<!-- gauge.js -->
<script>
    var opts = {
        lines: 12,
        angle: 0,
        lineWidth: 0.4,
        pointer: {
            length: 0.75,
            strokeWidth: 0.042,
            color: '#1D212A'
        },
        limitMax: 'false',
        colorStart: '#1ABC9C',
        colorStop: '#1ABC9C',
        strokeColor: '#F0F3F3',
        generateGradient: true
    };
    var target = document.getElementById('foo'),
        gauge = new Gauge(target).setOptions(opts);

    gauge.maxValue = 6000;
    gauge.animationSpeed = 32;
    gauge.set(3200);
    gauge.setTextField(document.getElementById("gauge-text"));
</script>
<!-- /gauge.js -->


<!-- bootstrap-wysiwyg -->
<script>
    $(document).ready(function () {
        function initToolbarBootstrapBindings() {
            var fonts = ['Serif', 'Sans', 'Arial', 'Arial Black', 'Courier',
                    'Courier New', 'Comic Sans MS', 'Helvetica', 'Impact', 'Lucida Grande', 'Lucida Sans', 'Tahoma', 'Times',
                    'Times New Roman', 'Verdana'
                ],
                fontTarget = $('[title=Font]').siblings('.dropdown-menu');
            $.each(fonts, function (idx, fontName) {
                fontTarget.append($('<li><a data-edit="fontName ' + fontName + '" style="font-family:\'' + fontName + '\'">' + fontName + '</a></li>'));
            });
            $('a[title]').tooltip({
                container: 'body'
            });
            $('.dropdown-menu input').click(function () {
                return false;
            })
                .change(function () {
                    $(this).parent('.dropdown-menu').siblings('.dropdown-toggle').dropdown('toggle');
                })
                .keydown('esc', function () {
                    this.value = '';
                    $(this).change();
                });

            $('[data-role=magic-overlay]').each(function () {
                var overlay = $(this),
                    target = $(overlay.data('target'));
                overlay.css('opacity', 0).css('position', 'absolute').offset(target.offset()).width(target.outerWidth()).height(target.outerHeight());
            });

            if ("onwebkitspeechchange" in document.createElement("input")) {
                var editorOffset = $('#editor').offset();

                $('.voiceBtn').css('position', 'absolute').offset({
                    top: editorOffset.top,
                    left: editorOffset.left + $('#editor').innerWidth() - 35
                });
            } else {
                $('.voiceBtn').hide();
            }
        }

        function showErrorAlert(reason, detail) {
            var msg = '';
            if (reason === 'unsupported-file-type') {
                msg = "Unsupported format " + detail;
            } else {
                console.log("error uploading file", reason, detail);
            }
            $('<div class="alert"> <button type="button" class="close" data-dismiss="alert">&times;</button>' +
                '<strong>File upload error</strong> ' + msg + ' </div>').prependTo('#alerts');
        }

        initToolbarBootstrapBindings();

        $('#editor').wysiwyg({
            fileUploadError: showErrorAlert
        });

        window.prettyPrint;
        prettyPrint();
    });
</script>
<!-- /bootstrap-wysiwyg -->

<!-- Select2 -->
<script>
    $(document).ready(function () {
        $(".select2_single").select2({
            placeholder: "Select a state",
            allowClear: true
        });
        $(".select2_group").select2({});
        $(".select2_multiple").select2({
            maximumSelectionLength: 4,
            placeholder: "With Max Selection limit 4",
            allowClear: true
        });
    });
</script>
<!-- /Select2 -->

<!-- jQuery Tags Input -->
<script>
    function onAddTag(tag) {
        alert("Added a tag: " + tag);
    }

    function onRemoveTag(tag) {
        alert("Removed a tag: " + tag);
    }

    function onChangeTag(input, tag) {
        alert("Changed a tag: " + tag);
    }

    $(document).ready(function () {
        $('#tags_1').tagsInput({
            width: 'auto'
        });
    });
</script>
<!-- /jQuery Tags Input -->

<!-- Parsley -->
<script>
    $(document).ready(function () {
        $.listen('parsley:field:validate', function () {
            validateFront();
        });
        $('#demo-form .btn').on('click', function () {
            $('#demo-form').parsley().validate();
            validateFront();
        });
        var validateFront = function () {
            if (true === $('#demo-form').parsley().isValid()) {
                $('.bs-callout-info').removeClass('hidden');
                $('.bs-callout-warning').addClass('hidden');
            } else {
                $('.bs-callout-info').addClass('hidden');
                $('.bs-callout-warning').removeClass('hidden');
            }
        };
    });

    $(document).ready(function () {
        $.listen('parsley:field:validate', function () {
            validateFront();
        });
        $('#demo-form2 .btn').on('click', function () {
            $('#demo-form2').parsley().validate();
            validateFront();
        });
        var validateFront = function () {
            if (true === $('#demo-form2').parsley().isValid()) {
                $('.bs-callout-info').removeClass('hidden');
                $('.bs-callout-warning').addClass('hidden');
            } else {
                $('.bs-callout-info').addClass('hidden');
                $('.bs-callout-warning').removeClass('hidden');
            }
        };
    });
    try {
        hljs.initHighlightingOnLoad();
    } catch (err) {
    }
</script>
<!-- /Parsley -->

<!-- Autosize -->
<script>
    $(document).ready(function () {
        autosize($('.resizable_textarea'));
    });
</script>
<!-- /Autosize -->

<!-- Starrr -->
<script>
    $(document).ready(function () {
        $(".stars").starrr();

        $('.stars-existing').starrr({
            rating: 4
        });

        $('.stars').on('starrr:change', function (e, value) {
            $('.stars-count').html(value);
        });

        $('.stars-existing').on('starrr:change', function (e, value) {
            $('.stars-count-existing').html(value);
        });
    });
</script>
<!-- /Starrr -->

<!-- Print Multiple Div -->
<script>
    function printDiv(divName) {
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;

        document.body.innerHTML = printContents;

        window.print();


        document.body.innerHTML = originalContents;

        location.reload();
    }
</script>


@yield('script')

</body>
</html>
