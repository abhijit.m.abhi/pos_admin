@extends('layouts.master')
@section('title', 'Add Expense')
@section('content')


    <script>
        $(function () {
            $("[name='expanse_category']").focus();
        });

        function CheckHead(val) {
            var stf_name = document.getElementById('staff_name');
            var type = document.getElementById('type');

            if (val == 'Staff Salary') {
                stf_name.style.display = 'block';
                type.style.display = 'none';
            }

            else {
                stf_name.style.display = 'none';
                type.style.display = 'block';

            }

        }

    </script>

    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Expense</h3>
                </div>

            </div>
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Add Expense<!--<small>Input different form elements</small>--></h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">

                            <form action="{{url('expanse')}}" method="POST" class="form-horizontal" role="form">
                                {{ csrf_field() }}
                                <div class="form-body">
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Select Category</label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="col-md-9" style="padding: 0px !important;">

                                                <select id="mySelect" name="expanse_category" class="form-control"
                                                        onchange='CheckHead(this.value);' required>
                                                    <option value="">Select Category</option>
                                                    <option value="Staff Salary">Staff Salary</option>
                                                    <option value="Rent">Rent</option>
                                                    <option value="Utility">Utility</option>
                                                    <option value="Transport">Transport</option>
                                                    <option value="Miscellaneous">Miscellaneous</option>
                                                    <option value="Daily Expanse">Daily Expense</option>
                                                    @foreach($categoryList as $showCategoryList)
                                                        <option value="{{$showCategoryList->categoryName}}">{{$showCategoryList->categoryName}}</option>
                                                    @endforeach

                                                </select>
                                            </div>
                                            <div class="col-md-3">
                                                <input type="hidden" id="token" value="{{csrf_token()}}"/>
                                                <button type="button" class="btn btn-success" data-toggle="modal"
                                                        data-target="#myModal" style="width: 114px;">Add New <span
                                                            class="glyphicon glyphicon-plus"></span></button>
                                            </div>


                                        </div>
                                    </div>

                                    <div class="form-group" id="type" style='display:none;'>
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Type</label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="text" class="form-control col-md-7 col-xs-12" name="type"
                                                   placeholder="Type">
                                        </div>
                                    </div>

                                    <div class="form-group" id="staff_name" style='display:none;'>
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Staff Name</label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="text" class="form-control col-md-7 col-xs-12" name="staff_name"
                                                   placeholder="Staff Name">
                                        </div>
                                    </div>

                                    <div class="form-group" style='display:block;'>
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Amount</label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="number" class="form-control col-md-7 col-xs-12" name="amount"
                                                   placeholder="Amount">
                                            @if ($errors->has('amount'))
                                                <span class="help-block">
                                                <strong>{{ $errors->first('amount') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="ln_solid"></div>
                                    <div class="form-group">
                                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                            <a type="cancel" href="{{ url('dashboard') }}"
                                               class="btn btn-danger">Cancel</a>
                                            <button type="submit" class="btn btn-primary">Add</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    {{--Modal--}}
    <div class="modal fade" tabindex="-1" id="myModal" aria-labelledby="myModalLabel" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header alert-success">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><span class="glyphicon glyphicon-plus-sign"></span> Add New Category</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="cat">New Category</label>
                        <input type="text" class="form-control" id="cat" placeholder="Type Here">
                    </div>
                    <input type="hidden" id="d_id">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                    <button type="button" class="savings btn btn-primary delete_product" data-dismiss="modal"
                            data-token="{{ csrf_token() }}" data-dismiss="modal">Add
                    </button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->


    {{--New Category Script--}}

    <script>
        $('.savings').click(
            function () {


                var newCat = $('#cat').val();

                $.ajax(
                    {
                        url: "add-new-expense",
                        dataType: "json",
                        type: "POST",
                        data: {
                            newCat: newCat,
                            _token: $('#token').val()
                        },
                        success: function (response) {
//
//                            if (response.status) {
//                                location.reload();
//                            }


                            $.ajax(
                                {
                                    url: "appendCat",
                                    dataType: "json",
                                    type: "POST",
                                    data: {
                                        _token: $('#token').val()
                                    },
                                    success: function (response) {

                                        console.log(response);

//                                            $.map(response, function(val, key) {
                                        console.log("Category: " + response.categoryName);

                                        var option = document.createElement("option");
                                        option.text = response.categoryName;
                                        option.value = response.categoryName;
                                        var select = document.getElementById("mySelect");
                                        select.appendChild(option);

//                                            });


                                    }
                                }
                            );

                        }
                    }
                );

                document.getElementById('cat').value = "";

            }
        );
    </script>

@endsection