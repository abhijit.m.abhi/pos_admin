@extends('layouts.master')
@section('title', 'Income Report')
@section('content')

    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Income <!--<small>Some examples to get you started</small>--></h3>
                </div>


            </div>

            <div class="clearfix"></div>

            <div class="row">

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Income Report <!--<small>Users</small>--></h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>


                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <form action="{{url('income/viewReport')}}" method="POST" class="form-horizontal"
                                      role="form">
                                    {{ csrf_field() }}


                                    <div class="row" style="padding: 0px !important; margin-left: -22px !important;">
                                        <fieldset>
                                            <div class="control-group col-md-12 col-lg-12">
                                                <div class="controls col-md-6 col-lg-6">
                                                    <div class="input-prepend input-group">
                                                        <span class="add-on input-group-addon"><i
                                                                    class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                                                        <input type="text" style="width: 200px" name="datePicker"
                                                               id="reservation" class="form-control" value=""/>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-lg-6">
                                                    <button type="submit" class="btn btn-primary">
                                                        <span class="glyphicon glyphicon-list-alt"></span> View Report
                                                    </button>
                                                </div>
                                            </div>

                                        </fieldset>

                                    </div>

                                </form>

                            </div>
                        </div>

                        <div class="x_content">

                            <div class="table-responsive">
                                <table id=""
                                       class="incomeAmount table table-striped table-bordered table-hover display">


                                    <thead>
                                    <tr class="alert-info">
                                        <th>Date</th>
                                        <th>Head</th>
                                        <th>Amount</th>
                                    </tr>
                                    </thead>

                                    <tfoot>
                                    <tr class="alert-success">
                                        <th>Total:</th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                    </tfoot>

                                    <tbody>
                                    @foreach($allIncomeList as $incomeList)
                                        <tr>
                                            <td>{{ date('m-d-Y',strtotime($incomeList->date)) }} </td>
                                            <td>{{$incomeList->head}}</td>
                                            <td>{{$incomeList->amount}}</td>
                                        </tr>

                                    @endforeach

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection
@section('script')

    <script>
        $(document).ready(function () {
            $("#datepicker").datepicker();
        });
    </script>

    <script>

        $('#reservation').daterangepicker(null, function (start, end, label) {
            console.log(start.toISOString(), end.toISOString(), label);
        });

        $('#reservation-time').daterangepicker({
            timePicker: true,
            timePickerIncrement: 30,
            locale: {
                format: 'MM/DD/YYYY h:mm A'
            }
        });

    </script>


@endsection