@extends('layouts.master')
@section('content')

    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>View Loan Installments</h3>
                </div>

            </div>
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2></h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="#">Settings 1</a>
                                        </li>
                                        <li><a href="#">Settings 2</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>


                        @if(Session::has('message'))

                            <div id="successMessage" class="alert alert-success alert-dismissible fade in" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                                </button>
                                <strong>{{ Session::get('message') }}</strong>
                            </div>
                        @endif

                        <form id="form" data-parsley-validate method="POST" class="form-horizontal" role="form">
                            {{ csrf_field() }}
                            <div class="form-body">

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Select Loan Id : </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select class="select2_single form-control loan" tabindex="-1" id="loan" name="loan_id">
                                            <option></option>

                                            @foreach($accountList as $accountList)
                                                <option value="{{$accountList->account_no}}">{{$accountList->account_no}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="demo" id="demo" style="display: none" >

                                    {{--  @foreach($installments as $person)
                                          <label >User ID {{$person->bank_name}}</label>


                                      @endforeach--}}

                                    {{--<label class="key">Person</label>--}}
                                    {{--<label class="data">{{ $person->branch_name }}</label>--}}
                                    <div class="col-md-12">

                                        <input type="hidden" class="form-control col-md-7 col-xs-9" readonly style="border: 0px;" name="id" id="id" >


                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Account Name : </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" class="form-control col-md-3 col-xs-12" readonly style="border: 0px;" name="account_name" id="account_name" >

                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Bank Name : </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" class="form-control col-md-3 col-xs-12" readonly style="border: 0px;" name="bank_name" id="bank_name" >

                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Loan Amount : </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" class="form-control col-md-7 col-xs-12" readonly style="border: 0px;" name="amount" id="amount" >

                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Loan Interest (%) : </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" class="form-control col-md-7 col-xs-12" readonly style="border: 0px;" name="percent" id="percent" >

                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Loan Start Date : </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="date" class="form-control col-md-7 col-xs-12" readonly style="border: 0px;" name="date" id="date" >

                                            </div>
                                        </div>


                                        {{--Add table view--}}

                                        <div class="container">

                                            <table class="viewInstallment table table-hover table-responsive table-striped table-bordered display " id="demo">


                                                <thead>
                                                <tr class="alert-info">
                                                    <th>Deposited By</th>
                                                    <th>Deposited Amount</th>
                                                    <th>Branch Name</th>
                                                    <th>Total Interest</th>
                                                    <th>Date</th>
                                                    <th>Action</th>

                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr id="">
                                                    <td></td>
                                                    <td id="deposit_amount"></td>
                                                    <td id="bank_branch"></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>

                                                </tr>

                                                <div class="modal fade" tabindex="-1" id="myModal" aria-labelledby="myModalLabel" role="dialog">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                                <h3 class="modal-title ">Delete Confirmation </h3>
                                                            </div>
                                                            <div class="modal-body ">
                                                                <p><b>Do you want to delete?&hellip;</b></p>
                                                                <input type="hidden" id="sid">
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-default warning" data-dismiss="modal">Close</button>
                                                                <button type="button " class="btn btn-primary delete_product btn-danger" data-dismiss="modal" value="" data-token="{{ csrf_token() }}"data-dismiss="modal">Delete</button>
                                                            </div>
                                                        </div><!-- /.modal-content -->
                                                    </div><!-- /.modal-dialog -->
                                                </div><!-- /.modal -->

                                                </tbody>
                                                <tfoot>
                                                <tr>
                                                    <th>Total:</th>
                                                    <th></th>
                                                    <th></th>
                                                    <th></th>
                                                    <th></th>
                                                    <th></th>

                                                </tr>
                                                </tfoot>
                                            </table>
                                        </div>



                                        {{--End table view--}}


                                    </div>





                                    <div class="ln_solid"></div>

                                </div>


                                <meta name="_token" content="{!! csrf_token() !!}" />
                            </div>
                        </form>


                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>

    <script>
        $(function () {
            $("#loan").on('change',function (e) {

                $("#demo").css("display","block");

                console.log(e);
                var selectedText = $(this).find("option:selected").text();
                var selectedValue = $(this).val();

                var url = "./add_installment";
                var url1 = "./add_details";
                $(".viewInstallment").find("tr:gt(0)").remove();
                $.ajaxSetup({
                    headers : {
                        'X-CSRF-TOKEN' : $('meta[name="_token"]').attr('content')
                    }

                })
                $.ajax({
                    type : 'GET',
                    url : url,
                    dataType : "json",
                    data:  {'id':selectedValue},
                    success: function(Response) {

                        console.log(Response);
                        var obj = JSON.parse(Response);
                        var data = obj[0];
                        //  ajaxResult.push(data.percent);
                        //  ajaxAmount.push(data.amount);

                        $("#demo").css("display","block");
                        $("#amount").val(data.amount);
                        $("#account_name").val(data.account_name);
                        $("#percent").val(data.percent);
                        $("#bank_name").val(data.bank_name);
                        $("#id").val(data.id);
                        $("#date").val(data.date);
                        $("#interest").val(0);


                        $.ajax({
                            type: 'GET',
                            url: url1,
                            dataType: "json",
                            data: {'id':  $("#id").val()},
                            success: function (Response) {



                                var json = JSON.parse(Response);

                                console.log(json);
                                for (var key in json) {
                                    if (json.hasOwnProperty(key)) {


                                        $('#demo tr:last').after('<tr id="myTable'+json[key].id+'" class="inst"> <td>'+json[key].deposited_by+'</td><td>'+json[key].deposit_amount+'</td><td>'+json[key].bank_branch+'</td><td>'+json[key].interest+'</td><td>'+json[key].date+'</td><td> <a href="installments/'+json[key].id+'/edit" class="btn btn-primary"><span class="glyphicon glyphicon-edit"></span></a><a href="#" class="btn confirm btn-danger delete_p" id="dl" data-id='+json[key].id+' onclick="myFunction('+json[key].id+')" ><span class="glyphicon glyphicon-trash"></span></a></td></tr>');


                                    }
                                }

                            }
                        })


                    }
                })
            })
        });


    </script>

    <script>
        function myFunction(did) {

            // var did = $(this).data('id');
            // $("#d_id").val(did);
            $('#myModal').modal('show');
             $('#sid').val(did);
        };



            $('.delete_product').click(function () {

                var id = $('#sid').val();
                var url = "delete";
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    }

                })
                $.ajax({
                    type: 'DELETE',
                    url: url + '/' + id,

                    success: function (data) {
                        console.log(id);
                        $("#myTable" + id).remove();

                    },
                    error: function (data) {
                        console.log('Error:', data);
                    }

                })

            })


    </script>

    <script>
        $(document).ready(function() {
            $(".select2_single").select2({
                placeholder: "Select a state",
                allowClear: true
            });
            $(".select2_group").select2({});
            $(".select2_multiple").select2({
                maximumSelectionLength: 4,
                placeholder: "With Max Selection limit 4",
                allowClear: true
            });
        });
    </script>


@endsection
