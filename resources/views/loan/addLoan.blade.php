@extends('layouts.master')
@section('content')

    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Add Loan</h3>
                </div>

            </div>
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Loan Record Registration</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="#">Settings 1</a>
                                        </li>
                                        <li><a href="#">Settings 2</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>


                        @if(Session::has('message'))

                            <div id="successMessage" class="alert alert-success alert-dismissible fade in" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                                </button>
                                <strong>{{ Session::get('message') }}</strong>
                            </div>
                        @endif



                        <div class="x_content">
                            <br />


                            <form id="form" data-parsley-validate action="{{route('loan.store')}}" method="POST" class="form-horizontal" role="form" files="true" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="form-body">
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Loan Id</label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="text" class="account_no form-control col-md-7 col-xs-12" placeholder="Account Number"  name="account_no" id="account_no" value="{{old('account_no')}}" required>
                                            @if ($errors->has('account_no'))
                                                <span class="help-block">
                                                <strong>{{ $errors->first('account_no') }}</strong>
                                            </span>
                                            @endif
                                            <span class="chk" id="chk" style="display: none;">
                                                <strong style="color:red">Name already used</strong>
                                                    <div col-md-6 col-sm-6 col-xs-12 id="search-result">

                                                    </div>
                                            </span>

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Account Name</label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="text" class="form-control col-md-7 col-xs-12" placeholder="Account Holder Name" name="account_name" id="account_name" value="{{old('account_name')}}" required>
                                            @if ($errors->has('account_name'))
                                                <span class="help-block">
                                                <strong>{{ $errors->first('account_name') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Bank Name</label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-envelope-o"></i>
                                    </span>
                                                <input type="text" name="bank_name" class="form-control col-md-7 col-xs-12" placeholder="Bank Name" id="bank_name" value="{{old('bank_name')}}" required>
                                            </div>
                                            @if ($errors->has('bank_name'))
                                                <span class="help-block">
                                                <strong>{{ $errors->first('bank_name') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Bank Branch Name</label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-envelope-o"></i>
                                    </span>
                                                <input type="text" name="branch_name"  class="form-control col-md-7 col-xs-12" placeholder="Branch Name" id="branch_name" value="{{old('branch_name')}}" >
                                            </div>
                                            @if ($errors->has('branch_name'))
                                                <span class="help-block">
                                                <strong>{{ $errors->first('branch_name') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Loan Amount</label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="number" name="amount" id="amount" class="form-control col-md-7 col-xs-12"
                                                   placeholder="Loan Amount"  value="{{old('amount')}}" required>
                                            @if ($errors->has('amount'))
                                                <span class="help-block">
                                                <strong>{{ $errors->first('amount') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Interest Rates  <span class="required">%</span></label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="number" name="percent" id="percent" class="percent form-control col-md-7 col-xs-12"
                                                   placeholder="Interest Rates"  value="{{old('percent')}}" >
                                            @if ($errors->has('percent'))
                                                <span class="help-block">
                                                <strong>{{ $errors->first('percent') }}</strong>
                                            </span>
                                            @endif
                                            <span class="per" id="per" style="display: none;">
                                                <strong style="color:red">Percent can not be more than 100</strong>

                                            </span>
                                        </div>
                                    </div>



                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Loan Start Date <span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">

                                            <input name="loan_start_date" class="birthday date-picker form-control col-md-7 col-xs-12" required="required" type="text">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Loan Duration</label>
                                        <div class="col-lg-4 ">
                                            <div class="row">
                                                <div class="col-lg-4 ">
                                                    <input type="text" name="year" id="year" onkeyup="C()" class="form-control col-md-7 "
                                                     placeholder="Year"  value="{{old('year')}}" >
                                                </div>
                                                <div class="col-lg-4 ">
                                                    <input type="text" name="month" id="month" onkeyup="generateFullDuration()"class="form-control col-md-7 col-xs-12"
                                                    placeholder="Month"  value="{{old('month')}}" >
                                                </div>
                                                <div class="col-lg-4 ">
                                                    <input type="text" name="date" id="date" onkeyup="generateFullDuration()"class="form-control col-md-7 col-xs-12"
                                                    placeholder="Day"  value="{{old('date')}}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Comment</label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <textarea rows="3" cols="20" class="form-control col-md-7 col-xs-12" placeholder="Comment Area......" name="comment" value="{{old('account_name')}}"></textarea>
                                            @if ($errors->has('comment'))
                                                <span class="help-block">
                                                <strong>{{ $errors->first('comment') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>


                                    <!--<div class="form-actions right">
                                        <button type="submit" class="btn green">Submit</button>
                                    </div>-->
                                    <div class="ln_solid"></div>
                                    <div class="form-group">
                                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                            <button  type="submit" class="btn btn-primary">Cancel</button>
                                            <button id="sub" type="submit" class="btn btn-success">Submit</button>
                                        </div>
                                    </div>
                                    <meta name="_token" content="{!! csrf_token() !!}" />
                                </div>
                            </form>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        // validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
        $('form')
            .on('blur', 'input[required], input.optional, select.required', validator.checkField)
            .on('change', 'select.required', validator.checkField)
            .on('keypress', 'input[required][pattern]', validator.keypress);

        $('.multi.required').on('keyup blur', 'input', function() {
            validator.checkField.apply($(this).siblings().last()[0]);
        });

        $('form').submit(function(e) {
            e.preventDefault();
            var submit = true;

            // evaluate the form using generic validaing
            if (!validator.checkAll($(this))) {
                submit = false;
            }

            if (submit)
                this.submit();

            return false;
        });

    </script>


    <script type="text/javascript">

        $('#account_no').on('blur', function() {

                var accountNo = $(this).val();
               // alert(accountNo);
                var url = "loan";
//                $.post('/loan/createAccount', {'account_no':account_no}, function(data) {
//                    console.log(data);
            $.ajaxSetup({
                headers : {
                    'X-CSRF-TOKEN' : $('meta[name="_token"]').attr('content')
                }

            })
            $.ajax({
                type : 'GET',
                url : '../check',
                data: {'account': accountNo},
                success: function(data){
                    if(data == "not")
                    {
                        //alert("not");
                        $("#chk").css("display", "block");
                       // $('#chk').show();
                        $('#sub').prop('disabled', true);
                    }

                    else
                    {
                        //alert("not");
                        $("#chk").css("display", "none");
                        // $('#chk').show();
                        $('#sub').prop('disabled', false);
                    }

                    //  $("#loan" + id).remove();
                },
                error:function(data){
                    console.log('Error:',data);
                }

            });

//
//
        });

//});
    </script>

    <script type="text/javascript">
        $( "#percent" ).blur(function() {

            var interest = $(this).val();
            if(interest>100)
            {
                //alert(interest);
                $("#per").css("display", "block");
                $('#sub').prop('disabled', true);
            }
            else {
                $("#per").css("display", "none");
                $('#sub').prop('disabled', false);

            }
        });

    </script>

@endsection
