@extends('layouts.master')
@section('content')

    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Loan <!--<small>Some examples to get you started</small>--></h3>
                </div>


            </div>

            <div class="clearfix"></div>

            <div class="row">

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Loan Report <!--<small>Users</small>--></h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>


                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <form action="{{url('viewinstallment/viewreport')}}" method="POST" class="form-horizontal"
                                      role="form">
                                    {{ csrf_field() }}

                                    <table>
                                        <tr>
                                            <td><input id="datepicker" name="start_date"
                                                       class="birthday date-picker form-control col-md-7 col-xs-12"
                                                       required="required" type="text"></td>
                                            <td><span class="input-group-addon">To</span></td>
                                            <td><input name="end_date"
                                                       class="birthday date-picker form-control col-md-7 col-xs-12"
                                                       required="required" type="text"></td>
                                            <td><input type="submit" id="" name="view_expense_report"
                                                       class="btn btn-primary" value="View Report"></td>
                                        </tr>
                                    </table>

                                </form>

                            </div>
                        </div>

                        {{--<div class="text-right"> <!--You can add col-lg-12 if you want -->
                            <button class="btn-info btn" id="buttons">Print</button>
                        </div>--}}
                        <div class="x_content">

                            <div class="table-responsive">
                                <table id="example" class="installmentTable table table-responsive table-striped table-bordered table-hover display">


                                    <thead class="table-inverse" >
                                    <tr>
                                        <th>Loan Id</th>
                                        <th>Account Name</th>
                                        <th>Bank Name</th>
                                        <th>Amount</th>
                                        <th>Interest Rate</th>
                                        <th>Deposited Amount</th>
                                        <th>Total Interest</th>
                                        <th>Date</th>

                                    </tr>
                                    </thead>

                                    <tfoot>
                                    <tr>
                                        <th>Total:</th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                    </tfoot>

                                    <tbody>

                                    @if($data1)

                                 @foreach($data1 as $tp)
                                        <tr >
                                            <td>{{$tp["accNo"]}}</td>
                                            <td>{{$tp["accName"]}}</td>
                                            <td>{{$tp["bankName"]}}</td>
                                            <td>{{$tp["amount"]}}</td>
                                            <td>{{$tp["percent"]}}%</td>
                                            <td>{{$tp["totalAmount"]}}</td>
                                            <td>{{$tp["totalInterest"]}}</td>
                                            <td>{{$tp["date"]}}</td>
                                        </tr>


                                    @endforeach

                                        @endif

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection
@section('script')

    <script>
        $(document).ready(function() {
            $("#datepicker").datepicker();
        });
    </script>
    <script>

        $('#reservation').daterangepicker(null, function(start, end, label) {
            console.log(start.toISOString(), end.toISOString(), label);
        });

        $('#reservation-time').daterangepicker({
            timePicker: true,
            timePickerIncrement: 30,
            locale: {
                format: 'MM/DD/YYYY h:mm A'
            }
        });

    </script>




@endsection