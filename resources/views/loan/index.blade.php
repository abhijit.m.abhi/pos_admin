@extends('layouts.master')
@section('content')
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Loan <!--<small>Some examples to get you started</small>--></h3>
                </div>

            </div>

            <div class="clearfix"></div>

            <div class="row">

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Loan List <!--<small>Users</small>--></h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>

                        @if(Session::has('message'))

                            <div id="successMessage" class="alert alert-success alert-dismissible fade in" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                                </button>
                                <strong>{{ Session::get('message') }}</strong>
                            </div>
                        @endif

                        <div class="x_content">
                            <!--<p class="text-muted font-13 m-b-30">
                                The Buttons extension for DataTables provides a common set of options, API methods and styling to display buttons on a page that will interact with a DataTable. The core library provides the based framework upon which plug-ins can built.
                            </p>-->
                            <div class="table-responsive">
                                <table id=""  class="loanAmount table table-striped table-bordered table-hover display" >
                                    <thead>
                                    <tr class="alert-info">
                                        <th>Loan Id</th>
                                        <th>Account Name</th>
                                        <th>Bank Name</th>
                                        <th>Bank Branch</th>
                                        <th>Amount</th>
                                        <th>Percent</th>
                                        <th>Date</th>
                                        <th>Loan Start Date</th>
                                        <th>Duration</th>
                                        <th>Comment</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tfoot>
                                    <tr>
                                        <th>Total:</th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                    </tfoot>

                                    <tbody>
                                    @foreach($loans as $loan)

                                        <tr id="loan{{$loan->id}}">
                                            <td>{{$loan->account_no}}</td>
                                            <td>{{$loan->account_name}}</td>
                                            <td>{{$loan->bank_name}}</td>
                                            <td>{{$loan->branch_name}}</td>


                                            <td>{{$loan->amount}}</td>
                                            <td>{{$loan->percent}}%</td>
                                            <td>{{$loan->date}}</td>
                                            <td>{{$loan->loan_start_date}}</td>
                                            <td>
                                                <?php
                                                $x = array();
                                                $duration = $loan->loan_duration;
                                                $x = json_decode($duration,true);
                                                echo $x[0]." Years". $x[1]." Months". $x[2]." Days";

                                                ?>

                                            </td>
                                            <td>{{$loan->comment}}</td>

                                            <td>

                                                <a href="{{route('loan.edit', $loan->id)}}" class="btn btn-primary"><span class="glyphicon glyphicon-edit"></span></a>
                                                {{--<form action="{{URL::to('/loan'.$loan->id)}}" method="post" id="choose">--}}
                                                {{--<input type="hidden" name="choose" value="{{$loan->id}}">--}}

                                                {{--<button type="button" class="btn btn-danger" onClick = "onDelete({{$loan->id}})" id="reco"><span class="glyphicon glyphicon-trash"></span></button>--}}
                                                <button class="btn confirm btn-danger delete_p"  data-toggle="modal" data-id="{{$loan->id}}"  ><span class="glyphicon glyphicon-trash"></span></button>
                                                {{--{{Form::token()}}--}}

                                            </td>
                                        </tr>
                                        {{--Modal--}}
                                        <div class="modal fade" tabindex="-1" id="myModal" aria-labelledby="myModalLabel" role="dialog">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                        <h3 class="modal-title ">Delete Confirmation </h3>
                                                    </div>
                                                    <div class="modal-body ">
                                                        <p><b>Do you want to delete?&hellip;</b></p>
                                                        <input type="hidden" id="d_id">
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-default warning" data-dismiss="modal">Close</button>
                                                        <button type="button " class="btn btn-primary delete_product btn-danger" data-dismiss="modal" value="" data-token="{{ csrf_token() }}"data-dismiss="modal">Delete</button>
                                                    </div>
                                                </div><!-- /.modal-content -->
                                            </div><!-- /.modal-dialog -->
                                        </div><!-- /.modal -->


                                    @endforeach
                                    <meta name="_token" content="{!! csrf_token() !!}" />

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>


@endsection
@section('script')

    <script>

        setTimeout(function(){
            $("#successMessage").fadeOut('slow');
        }, 3000);

    </script>




    <script type="text/javascript">
        // $('.confirm').confirm(
        $(document).on("click", ".delete_p", function() {
            var did = $(this).data('id');
            $("#d_id").val(did);
            $('#myModal').modal('show');
        });
        var url = "loan";

        $('.delete_product').click(function(){
            var _id =  $("#d_id").val();

            //alert(_id);

            var id = _id;

            $.ajaxSetup({
                headers : {
                    'X-CSRF-TOKEN' : $('meta[name="_token"]').attr('content')
                }

            })
            $.ajax({
                type : 'DELETE',
                url : url + '/' + id,
                success: function(data){
                    console.log(id);

                    $("#loan" + id).remove();
                },
                error:function(data){
                    console.log('Error:',data);
                }

            });
        });

        //        });
        //);
    </script>



@endsection