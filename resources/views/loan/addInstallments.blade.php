@extends('layouts.master')
@section('content')

    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Add Loan Installments</h3>
                </div>

            </div>
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2></h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="#">Settings 1</a>
                                        </li>
                                        <li><a href="#">Settings 2</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>


                        @if(Session::has('message'))

                            <div id="successMessage" class="alert alert-success alert-dismissible fade in" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                                </button>
                                <strong>{{ Session::get('message') }}</strong>
                            </div>
                        @endif



                        <div class="x_content">
                            <br />


                            <form id="form" data-parsley-validate action="{{route('installments.store')}}" method="POST" class="form-horizontal" role="form" files="true" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="form-body">

                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Select Loan Id : </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <select class="select2_single form-control loan" tabindex="-1" id="loan" name="loan_id">
                                                <option></option>

                                                @foreach($accountList as $accountList)
                                                     <option value="{{$accountList->account_no}}">{{$accountList->account_no}}</option>
                                                    @endforeach
                                            </select>
                                        </div>
                                    </div>
                                <div class="demo" id="demo" style="display: none">

                                  {{--  @foreach($installments as $person)
                                        <label >User ID {{$person->bank_name}}</label>


                                    @endforeach--}}

                                        {{--<label class="key">Person</label>--}}
                                        {{--<label class="data">{{ $person->branch_name }}</label>--}}
                                    <div class="col-md-12">

                                                    <input type="hidden" class="form-control col-md-7 col-xs-9" readonly style="border: 0px;" name="id" id="id" >


                                            <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Account Name : </label>
                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                    <input type="text" class="form-control col-md-3 col-xs-12" readonly style="border: 0px;" name="account_name" id="account_name" >

                                                </div>
                                            </div>
                                        <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Bank Name : </label>
                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                    <input type="text" class="form-control col-md-3 col-xs-12" readonly style="border: 0px;" name="bank_name" id="bank_name" >

                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Loan Amount : </label>
                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                    <input type="text" class="form-control col-md-7 col-xs-12" readonly style="border: 0px;" name="amount" id="amount" >

                                                </div>
                                            </div>
                                        <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Loan Interest (%) : </label>
                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                    <input type="text" class="form-control col-md-7 col-xs-12" readonly style="border: 0px;" name="percent" id="percent" >

                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Loan Start Date : </label>
                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                    <input type="date" class="form-control col-md-7 col-xs-12" readonly style="border: 0px;" name="date" id="date" >

                                                </div>
                                            </div>
                                    </div>
                                            {{--form data entry add loan installment--}}

                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Installment Amount : </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="number" class="form-control col-md-7 col-xs-12 deposit_amount" placeholder="Installment Amount" name="deposit_amount"  id="deposit_amount" onkeyup="myFunction()" onchange="hello()" value="{{old('deposit_amount')}}" required>
                                            @if ($errors->has('deposit_amount'))
                                                <span class="help-block">
                                                <strong>{{ $errors->first('deposit_amount') }}</strong>
                                            </span>
                                            @endif
                                            <span class="checkAmount" id="checkAmount" style="display: none;">
                                                <strong style="color:red">Deposited Amount is greater than total amount</strong>
                                                    <div col-md-6 col-sm-6 col-xs-12 id="search-result">

                                                    </div>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Interest Amount : </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="text" class="form-control col-md-7 col-xs-12" readonly style="border: 0px;" name="interest" id="interest" >
                                            @if ($errors->has('interest'))
                                                <span class="help-block">
                                                <strong>{{ $errors->first('interest') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Branch Name :</label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-envelope-o"></i>
                                    </span>
                                                <input type="text" name="bank_branch"  class="form-control col-md-7 col-xs-12" placeholder="Branch Name" id="bank_branch" value="{{old('bank_branch')}}" required>
                                            </div>
                                            @if ($errors->has('bank_branch'))
                                                <span class="help-block">
                                                <strong>{{ $errors->first('bank_branch') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Depositor Name : </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="text" class="form-control col-md-7 col-xs-12" placeholder="Depositor Name" name="deposited_by" id="deposited_by" value="{{old('deposited_by')}}" required>
                                            @if ($errors->has('deposited_by'))
                                                <span class="help-block">
                                                <strong>{{ $errors->first('deposited_by') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Date : <span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">

                                            <input name="date" class="birthday date-picker form-control col-md-7 col-xs-12" required="required" type="text">
                                        </div>
                                    </div>

                                    <div class="ln_solid"></div>
                                    <div class="form-group">
                                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                            <button  type="submit" class="btn btn-primary">Cancel</button>
                                            <button id="submit" type="submit" class="btn btn-success">Submit</button>
                                        </div>
                                    </div>
                                </div>


                                    <meta name="_token" content="{!! csrf_token() !!}" />
                                </div>
                            </form>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
       // var ajaxResult=[];
       // var ajaxAmount=[];
        $(function () {
            $("#loan").on('change',function (e) {
                $("#deposit_amount").val(null);
                $("#interest").val(null);
                console.log(e);
                var selectedText = $(this).find("option:selected").text();
                var selectedValue = $(this).val();
                var url = "../addinstallment";
                $.ajaxSetup({
                    headers : {
                        'X-CSRF-TOKEN' : $('meta[name="_token"]').attr('content')
                    }

                })
                $.ajax({
                    type : 'GET',
                    url : url,
                    dataType : "json",
                    data:  {'id':selectedValue},
                    success: function(Response) {

                        console.log(Response);
                        var obj = JSON.parse(Response);
                        var data = obj[0];
                      //  ajaxResult.push(data.percent);
                      //  ajaxAmount.push(data.amount);

                        $("#demo").css("display","block");
                        $("#amount").val(data.amount);
                        $("#account_name").val(data.account_name);
                        $("#percent").val(data.percent);
                        $("#bank_name").val(data.bank_name);
                        $("#id").val(data.id);
                        $("#date").val(data.date);
                        $("#interest").val(0);


                    }
                })
            })
        });

    </script>
    <script>
        $(document).ready(function() {
            $(".select2_single").select2({
                placeholder: "Select a state",
                allowClear: true
            });
            $(".select2_group").select2({});
            $(".select2_multiple").select2({
                maximumSelectionLength: 4,
                placeholder: "With Max Selection limit 4",
                allowClear: true
            });
        });
    </script>
    <script type="text/javascript">
        function myFunction() {
            var deposit = $("#deposit_amount").val();

                ajaxResult = $("#percent").val();
                var totalInterest = ((deposit) * ((ajaxResult) / (100))).toFixed(2);
                $("#interest").val(totalInterest);

        }

    </script>

    <script type="text/javascript">
        // var x = document.getElementById("deposit_amount").value;
        $(document).ready(function() {
        $('.deposit_amount').on('blur',function() {
            var x = +$("#deposit_amount").val();
            var y =  +$("#amount").val();
          //  alert(y);

            if(x>y){
                $("#checkAmount").css("display", "block");
                // $('#chk').show();
                $('#submit').prop('disabled', true);

            }
            else{


                $("#checkAmount").css("display", "none");
                // $('#chk').show();
                $('#submit').prop('disabled', false);

            }
        });
        });


       /*
        function hello() {
            //var deposit = $("#deposit_amount").val();
            var x = document.getElementById("deposit_amount").value;
            var y = document.getElementById("amount").value;

            if(x>y){
                $("#checkAmount").css("display", "block");
                // $('#chk').show();
                $('#submit').prop('disabled', true);

            }
            else{


                $("#checkAmount").css("display", "none");
                // $('#chk').show();
                $('#submit').prop('disabled', false);

            }


        }*/





         // alert(y);


    </script>



    <script>
        // validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
        $('form')
            .on('blur', 'input[required], input.optional, select.required', validator.checkField)
            .on('change', 'select.required', validator.checkField)
            .on('keypress', 'input[required][pattern]', validator.keypress);



        $('.multi.required').on('keyup blur', 'input', function() {
            validator.checkField.apply($(this).siblings().last()[0]);
        });

        $('form').submit(function(e) {
            e.preventDefault();
            var submit = true;

            // evaluate the form using generic validaing
            if (!validator.checkAll($(this))) {
                submit = false;
            }

            if (submit)
                this.submit();

            return false;
        });

    </script>

@endsection
