@extends('layouts.master')
@section('title', 'View Report')
@section('content')

    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Report <!--<small>Some examples to get you started</small>--></h3>
                </div>


            </div>

            <div class="clearfix"></div>

            <div class="row">

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>View Report <!--<small>Users</small>--></h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>


                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <form action="{{url('report/viewReport')}}" method="POST" class="form-horizontal"
                                      role="form">
                                    {{ csrf_field() }}


                                    <div class="row" style="padding: 0px !important; margin-left: -14px !important;">
                                        <fieldset>
                                            <div class="control-group col-md-12 col-lg-12">
                                                <div class="controls col-md-6 col-lg-6">
                                                    <div class="input-prepend input-group">
                                                        <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                                                        <input type="text" style="width: 200px" name="datePicker" id="reservation" class="form-control" value="" />
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-lg-6">
                                                    <button type="submit" class="btn btn-primary">
                                                        <span class="glyphicon glyphicon-list-alt"></span> View Report
                                                    </button>
                                                </div>
                                            </div>

                                        </fieldset>

                                    </div>

                                </form>

                            </div>
                        </div>

                        <button type="button" class="btn btn-danger btn-sm pull-right" onclick="printDiv('printableArea')">
                            <span class="glyphicon glyphicon-print"></span> Print
                        </button>
                        <div class="x_content" style="padding: 0px !important; " id="printableArea">



                            <div class="table-responsive col-md-6 col-sm-6 col-xs-12">
                                <h2>Income Report</h2>
                                <table id=""
                                       class="incomeAmountReport table table-striped table-bordered table-hover display">


                                    <thead>
                                    <tr class="alert-info">
                                        <th>Date</th>
                                        <th>Head</th>
                                        <th>Amount</th>
                                    </tr>
                                    </thead>

                                    <tfoot>
                                    <tr class="alert-success">
                                        <th>Total:</th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                    </tfoot>

                                    <tbody>
                                    @foreach($allIncomeList as $incomeList)
                                        <tr>
                                            <td>{{ date('m-d-Y',strtotime($incomeList->date)) }} </td>
                                            <td>{{$incomeList->head}}</td>
                                            <td>{{$incomeList->amount}}</td>

                                        </tr>


                                    @endforeach

                                    </tbody>
                                </table>
                            </div>

                            <div class="table-responsive col-md-6 col-sm-6 col-xs-12">
                                <h2>Expense Report</h2>
                                <table id=""
                                       class="expanseAmountReport table table-striped table-bordered table-hover display">


                                    <thead>
                                    <tr class="alert-info">
                                        <th>Date</th>
                                        <th>Category</th>
                                        <th>Head</th>
                                        <th>Amount</th>
                                    </tr>
                                    </thead>

                                    <tfoot>
                                    <tr class="alert-success">
                                        <th>Total:</th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                    </tfoot>

                                    <tbody>

                                    @foreach($allExpanseList as $expanseList)
                                        <tr>
                                            <td>{{ date('m-d-Y',strtotime($expanseList->date)) }} </td>
                                            <td>{{$expanseList->category}}</td>
                                            <td>{{$expanseList->head}}</td>
                                            <td>{{$expanseList->amount}}</td>
                                        </tr>
                                    @endforeach

                                    </tbody>
                                </table>
                            </div>

                            <div class="table-responsive col-md-12 col-sm-12 col-xs-12">
                                <h2>Balance Report</h2>
                                <table id=""
                                       class="balanceAmountReport table table-striped table-bordered table-hover display">

                                    <thead>
                                    <tr class="alert-info">
                                        <th>From</th>
                                        <th>To</th>
                                        <th>Income</th>
                                        <th>Expense</th>
                                        @if(($incomeTotal)<$expanseTotal)
                                            <th class="alert-danger">Extra Expense</th>
                                        @else
                                            <th>Balance</th>
                                        @endif
                                    </tr>
                                    </thead>


                                    <tbody>
                                    <tr>
                                        <td>{{$s_date or ''}}</td>
                                        <td>{{$e_date or ''}}</td>
                                        <td>{{$incomeTotal}}</td>
                                        <td>{{$expanseTotal}}</td>
                                        <td>{{abs($totalBalance)}}</td>
                                    </tr>


                                    </tbody>
                                </table>
                            </div>


                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection
@section('script')

    <script>
        $(document).ready(function () {
            $("#datepicker").datepicker();
        });
    </script>

    <script>

        $('#reservation').daterangepicker(null, function(start, end, label) {
            console.log(start.toISOString(), end.toISOString(), label);
        });

        $('#reservation-time').daterangepicker({
            timePicker: true,
            timePickerIncrement: 30,
            locale: {
                format: 'MM/DD/YYYY h:mm A'
            }
        });

    </script>



@endsection