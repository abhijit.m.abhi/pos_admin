@extends('layouts.master')
@section('title', 'Add Budget')
@section('content')


    <script>
        $(function () {
            $("[name='budget_category']").focus();
        });

        function CheckHead(val) {
            var monthlyDate = document.getElementById('for_monthly');
            var customDate = document.getElementById('for_custom');

            if (val == 'Custom') {
                customDate.style.display = 'block';
                monthlyDate.style.display = 'none';
            }

            else{
                customDate.style.display = 'none';
                monthlyDate.style.display = 'block';
                $("#birthday").val("");

            }

        }

    </script>

    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Budget</h3>
                </div>

            </div>
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Add Budget<!--<small>Input different form elements</small>--></h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">

                            <form action="{{url('budget')}}" method="POST" class="form-horizontal" role="form">
                                {{ csrf_field() }}
                                <div class="form-body">

                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Budget Type</label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <div id="budget_type" class="btn-group" data-toggle="buttons">
                                                <label class="btn btn-default" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                                                    <input type="radio" name="budget_type" value="Monthly" onchange='CheckHead(this.value);'> &nbsp; Monthly &nbsp;
                                                </label>
                                                <label class="btn btn-primary" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                                                    <input type="radio" name="budget_type" value="Custom" onchange='CheckHead(this.value);'> Custom
                                                </label>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="form-group" id="for_monthly">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Month <span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-xs-12 xdisplay_inputx form-group has-feedback">
                                            <input type="text" class="form-control has-feedback-left" name="month_year" id="birthday" placeholder="Select Month" aria-describedby="inputSuccess2Status2">
                                            <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                            <span id="inputSuccess2Status2" class="sr-only">(success)</span>
                                        </div>
                                    </div>

                                    <div class="form-group" id="for_custom" style="display: none">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Date<span class="required">*</span></label>
                                        <div class="col-lg-6 ">
                                            <div class="row">
                                                <div class="col-lg-5 ">
                                                    <input name="start_date" class="birthday date-picker form-control" required="required" type="text">
                                                </div>
                                                <div class="col-lg-2 ">
                                                    <span class="input-group-addon">To</span>
                                                </div>
                                                <div class="col-lg-5 ">
                                                    <input name="end_date" class="birthday date-picker form-control" required="required" type="text">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Select Category<span class="required">*</span></label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="col-md-9" style="padding: 0px !important;">

                                                <select id="mySelect" name="budget_category" class="form-control" required>
                                                    <option value="">Select Category</option>

                                                    @foreach($categoryList as $showCategoryList)
                                                        <option value="{{$showCategoryList->categoryName}}">{{$showCategoryList->categoryName}}</option>
                                                    @endforeach

                                                </select>
                                            </div>
                                            <div class="col-md-3">
                                                <input type="hidden" id="token" value="{{csrf_token()}}"/>
                                                <button type="button" class="btn btn-success" data-toggle="modal"
                                                        data-target="#myModal" style="width: 114px;">Add New <span
                                                            class="glyphicon glyphicon-plus"></span></button>
                                            </div>


                                        </div>
                                    </div>

                                    <div class="form-group" id="title" style='display:block;'>
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Title</label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="text" class="form-control col-md-7 col-xs-12" name="title"
                                                   placeholder="Title">
                                        </div>
                                    </div>



                                    <div class="form-group" style='display:block;'>
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Amount<span class="required">*</span></label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="number" class="form-control col-md-7 col-xs-12" name="amount"
                                                   placeholder="Amount">
                                            @if ($errors->has('amount'))
                                                <span class="help-block">
                                                <strong>{{ $errors->first('amount') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group" style='display:block;'>
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Extend Amount</label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="number" class="form-control col-md-7 col-xs-12" name="extend_amount"
                                                   placeholder="Extend Amount">
                                            @if ($errors->has('extend_amount'))
                                                <span class="help-block">
                                                <strong>{{ $errors->first('extend_amount') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Comment</label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <textarea rows="3" cols="20" class="form-control col-md-7 col-xs-12" placeholder="Comment Area......" name="comment" value="{{old('account_name')}}"></textarea>
                                            @if ($errors->has('comment'))
                                                <span class="help-block">
                                                <strong>{{ $errors->first('comment') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="ln_solid"></div>
                                    <div class="form-group">
                                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                            <a type="cancel" href="{{ url('dashboard') }}"
                                               class="btn btn-danger">Cancel</a>
                                            <button type="submit" class="btn btn-primary">Add</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    {{--Modal--}}
    <div class="modal fade" tabindex="-1" id="myModal" aria-labelledby="myModalLabel" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header alert-success">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><span class="glyphicon glyphicon-plus-sign"></span> Add New Category</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="cat">New Category</label>
                        <input type="text" class="form-control" id="cat" placeholder="Type Here">
                    </div>
                    <input type="hidden" id="d_id">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                    <button type="button" class="savings btn btn-primary delete_product" data-dismiss="modal"
                            data-token="{{ csrf_token() }}" data-dismiss="modal">Add
                    </button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->


    {{--New Category Script--}}


    <script>
        $('.savings').click(
            function () {
                var newCat = $('#cat').val();
                $.ajax(
                    {
                        url: "add-new-budget",
                        dataType: "json",
                        type: "POST",
                        data: {
                            newCat: newCat,
                            _token: $('#token').val()
                        },
                        success: function (response) {
                            $.ajax(
                                {
                                    url: "append-new-cat",
                                    dataType: "json",
                                    type: "POST",
                                    data: {
                                        _token: $('#token').val()
                                    },
                                    success: function (response) {

                                        console.log(response);

//                                            $.map(response, function(val, key) {
                                        console.log("Category: " + response.categoryName);

                                        var option = document.createElement("option");
                                        option.text = response.categoryName;
                                        option.value = response.categoryName;
                                        var select = document.getElementById("mySelect");
                                        select.appendChild(option);

//                                            });


                                    }
                                }
                            );

                        }
                    }
                );

                document.getElementById('cat').value = "";

            }
        );
    </script>

    <script>
        $(document).ready(function() {
            $("#birthday").datepicker( {
                format: "MM yyyy",
                viewMode: "months",
                minViewMode: "months",
                autoclose: true
            });
        });
    </script>



@endsection