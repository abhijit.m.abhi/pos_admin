@extends('layouts.master')
@section('title', 'Edit Budget')
@section('content')

    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Budget</h3>
                </div>
            </div>

            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Edit Budget <!--<small>Edit different form elements</small>--></h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                       aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="#">Settings 1</a>
                                        </li>
                                        <li><a href="#">Settings 2</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <br/>


                            <form action="{{url('budget/'.$budget->id)}}" method="POST" class="form-horizontal"
                                  role="form"
                                  enctype="multipart/form-data">
                                {{ method_field('PUT') }}
                                {{ csrf_field() }}
                                <div class="form-body">

                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Date<span class="required">*</span></label>
                                        <div class="col-md-7">
                                            <div class="row">
                                                <?php
                                                $start_date = date('m-d-Y',strtotime($budget->start_date));
                                                $end_date = date('m-d-Y',strtotime($budget->end_date));
                                                ?>
                                                <div class="col-md-5 ">
                                                    <input name="start_date" class="birthday date-picker form-control"  value="{{$start_date}}" type="text">
                                                </div>
                                                <div class="col-md-2 ">
                                                    <span class="input-group-addon">To</span>
                                                </div>
                                                <div class="col-md-5 ">
                                                    <input name="end_date" class="birthday date-picker form-control" value="{{$end_date}}" type="text">
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Select Category</label>
                                        <div class="col-md-7 col-sm-7 col-xs-12">
                                            <select id="mySelect" name="budget_category" class="form-control">
                                                <option value="{{$budget->category}}">{{$budget->category}}</option>
                                                @foreach($categoryList as $showCategoryList)
                                                    <option value="{{$showCategoryList->categoryName}}">{{$showCategoryList->categoryName}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Title</label>
                                        <div class="col-md-7">
                                            <input type="text" class="form-control col-md-7 col-xs-12"
                                                   placeholder="Title" name="title"
                                                   value="{{$budget->title}}">
                                            @if ($errors->has('title'))
                                                <span class="help-block">
                                                <strong>{{ $errors->first('title') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Amount</label>
                                        <div class="col-md-7">
                                            <input type="number" class="form-control col-md-7 col-xs-12"
                                                   placeholder="Amount" name="amount"
                                                   value="{{$budget->amount}}">
                                            @if ($errors->has('amount'))
                                                <span class="help-block">
                                                <strong>{{ $errors->first('amount') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Extend Amount</label>
                                        <div class="col-md-7">
                                            <input type="number" class="form-control col-md-7 col-xs-12"
                                                   placeholder="Extend Amount" name="extend_amount"
                                                   value="{{$budget->extend_amount}}">
                                            @if ($errors->has('extend_amount'))
                                                <span class="help-block">
                                                <strong>{{ $errors->first('extend_amount') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Comment</label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <textarea rows="3" cols="20" class="form-control col-md-7 col-xs-12" placeholder="Comment Area......" name="comment" >{{$budget->comment}}</textarea>
                                            @if ($errors->has('comment'))
                                                <span class="help-block">
                                                <strong>{{ $errors->first('comment') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>


                                    <div class="form-actions right">
                                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                            <button type="submit" class="btn btn-success">Update</button>
                                            <a type="cancel" href="{{ url('dashboard') }}" class="btn btn-primary">Cancel</a>
                                        </div>
                                    </div>
                                </div>
                            </form>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection