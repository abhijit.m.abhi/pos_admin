@extends('layouts.master')
@section('title', 'Add User')
@section('content')

    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>User</h3>
                </div>


            </div>
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>User Registration<!--<small>Input different form elements</small>--></h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <br/>

                            <form action="{{url('user')}}" method="POST" class="form-horizontal" role="form"
                                  files="true" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="form-body">
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">First Name</label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="text" class="form-control col-md-7 col-xs-12"
                                                   placeholder="First Name" name="fname" value="{{old('fname')}}">
                                            @if ($errors->has('fname'))
                                                <span class="help-block">
                                                <strong>{{ $errors->first('fname') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Last Name</label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="text" class="form-control col-md-7 col-xs-12"
                                                   placeholder="Last Name" name="lname" value="{{old('lname')}}">
                                            @if ($errors->has('lname'))
                                                <span class="help-block">
                                                <strong>{{ $errors->first('lname') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Email</label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-envelope-o"></i>
                                    </span>
                                                <input type="email" name="email" class="form-control col-md-7 col-xs-12"
                                                       placeholder="Email Address" value="{{old('email')}}">
                                            </div>
                                            @if ($errors->has('email'))
                                                <span class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Address</label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="text" name="address" class="form-control col-md-7 col-xs-12"
                                                   placeholder="Address" value="{{old('address')}}">
                                            @if ($errors->has('address'))
                                                <span class="help-block">
                                                <strong>{{ $errors->first('address') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">User Type</label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            {!! Form::select('type', array('admin' => 'Admin', 'user' => 'User'), null, ['class'=>'form-control col-md-7 col-xs-12','placeholder' => 'Choose a Type']) !!}
                                            @if ($errors->has('type'))
                                                <span class="help-block">
                                                <strong>{{ $errors->first('type') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">User Status</label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            {!! Form::select('status', array('active' => 'Active', 'deactive' => 'Deactive'), null, ['class'=>'form-control col-md-7 col-xs-12','placeholder' => 'Choose a Status']) !!}
                                            @if ($errors->has('status'))
                                                <span class="help-block">
                                                <strong>{{ $errors->first('status') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputFile" class="control-label col-md-3 col-sm-3 col-xs-12">Picture</label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            {!! Form::file('photo') !!}
                                            @if ($errors->has('photo'))
                                                <span class="help-block">
                                                <strong>{{ $errors->first('photo') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Password</label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-lock"></i>
                                </span>
                                                <input name="password" type="password"
                                                       class="form-control col-md-7 col-xs-12" placeholder="Password">
                                            </div>
                                            @if ($errors->has('password'))
                                                <span class="help-block">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Re-Password</label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-lock"></i>
                                    </span>
                                                <input name="password_confirmation" type="password"
                                                       class="form-control col-md-7 col-xs-12"
                                                       placeholder="Password">
                                            </div>
                                            @if ($errors->has('password_confirmation'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="ln_solid"></div>
                                    <div class="form-group">
                                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                            <a type="cancel" href="{{ url('dashboard') }}"
                                               class="btn btn-danger">Cancel</a>
                                            <button type="submit" class="btn btn-primary">Add</button>
                                        </div>
                                    </div>

                                </div>
                            </form>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>




@endsection