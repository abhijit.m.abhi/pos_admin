@extends('layouts.master')
@section('title', 'Expense List')
@section('content')
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Expense <!--<small>Some examples to get you started</small>--></h3>
                </div>

            </div>

            <div class="clearfix"></div>

            <div class="row">

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Expense List <!--<small>Users</small>--></h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>

                        @if(Session::has('message'))

                            <div id="successMessage" class="alert alert-success alert-dismissible fade in" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                            aria-hidden="true">×</span>
                                </button>
                                <strong>{{ Session::get('message') }}</strong>
                            </div>
                        @endif

                        <div class="x_content">
                            <!--<p class="text-muted font-13 m-b-30">
                                The Buttons extension for DataTables provides a common set of options, API methods and styling to display buttons on a page that will interact with a DataTable. The core library provides the based framework upon which plug-ins can built.
                            </p>-->
                            <div class="table-responsive">
                                <table id="" class="example table table-striped table-bordered table-hover display">
                                    <thead>
                                    <tr class="alert-info">
                                        <th>Date</th>
                                        <th>Category</th>
                                        <th>Head</th>
                                        <th>Amount</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>


                                    <tbody>
                                    @foreach($allExpanseList as $expanseList)

                                        <tr id="expanse{{$expanseList->id}}">
                                            <td>{{ date('m-d-Y',strtotime($expanseList->date)) }} </td>
                                            <td>{{$expanseList->category}}</td>
                                            <td>{{$expanseList->head}}</td>
                                            <td>{{$expanseList->amount}}</td>
                                            <td align="center">
                                                <a href="{{route('expanse.edit', $expanseList->id)}}"
                                                   class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-edit"></span></a>
                                                <button class="btn confirm btn-danger btn-sm delete_p" data-toggle="modal" data-id="{{$expanseList->id}}"><span class="glyphicon glyphicon-trash"></span></button>
                                            </td>
                                        </tr>

                                        {{--Modal--}}
                                        <div class="modal fade" tabindex="-1" id="myModal" aria-labelledby="myModalLabel"
                                             role="dialog">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header alert-danger">
                                                        <button type="button" class="close" data-dismiss="modal"
                                                                aria-label="Close"><span aria-hidden="true">&times;</span>
                                                        </button>
                                                        <h4 class="modal-title"><span class="glyphicon glyphicon-trash"></span>
                                                            Confirmation Message</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <p>Are you sure that you want to permanently delete the selected
                                                            element?</p>
                                                        <input type="hidden" id="d_id">
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-warning" data-dismiss="modal">No
                                                        </button>
                                                        <button type="button" class="btn btn-danger delete_product"
                                                                data-dismiss="modal" value="{{$expanseList->id}}"
                                                                data-token="{{ csrf_token() }}" data-dismiss="modal">Delete
                                                        </button>
                                                    </div>
                                                </div><!-- /.modal-content -->
                                            </div><!-- /.modal-dialog -->
                                        </div><!-- /.modal -->

                                    @endforeach

                                    </tbody>
                                    <tfoot>
                                    <tr class="alert-success">
                                        <th>Total:</th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                    </tfoot>
                                </table>



                            </div>
                        </div>
                        <meta name="_token" content="{!! csrf_token() !!}"/>
                    </div>
                </div>

            </div>
        </div>
    </div>


@endsection
@section('script')

    <script>

        setTimeout(function () {
            $("#successMessage").fadeOut('slow');
        }, 3000);

    </script>
    <script type="text/javascript">
        // $('.confirm').confirm(
        $(document).on("click", ".delete_p", function () {
            var did = $(this).data('id');
            $("#d_id").val(did);
            $('#myModal').modal('show');
        });
        var url = "expanse";
        $('.delete_product').click(function () {
            var _id = $("#d_id").val();
            //alert(_id);
            console.log("ok");
            var id = _id;
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            })
            $.ajax({
                type: 'DELETE',
                url: url + '/' + id,
                success: function (data) {
                    console.log(id);
                    $("#expanse" + id).remove();
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        });
        //        });
        //);
    </script>


@endsection