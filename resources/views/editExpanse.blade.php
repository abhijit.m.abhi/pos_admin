@extends('layouts.master')
@section('title', 'Edit Expense')
@section('content')

    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Expanse</h3>
                </div>
            </div>

            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Edit Expanse <!--<small>Edit different form elements</small>--></h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                       aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="#">Settings 1</a>
                                        </li>
                                        <li><a href="#">Settings 2</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <br/>


                            <form action="{{url('expanse/'.$expanse->id)}}" method="POST" class="form-horizontal"
                                  role="form"
                                  enctype="multipart/form-data">
                                {{ method_field('PUT') }}
                                {{ csrf_field() }}
                                <div class="form-body">


                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Category</label>
                                        <div class="col-md-7">
                                            {!! Form::select('category', array('staff_salary' => 'Staff Salary', 'rent' => 'Rent', 'utility' => 'Utility',  'transport' => 'Transport', 'miscellaneous' => 'Miscellaneous', 'daily_expanse' => 'Daily Expanse', ), $expanse->category, ['class'=>'form-control col-md-7 col-xs-12','placeholder' => 'Choose a Category']) !!}
                                            @if ($errors->has('category'))
                                                <span class="help-block">
                                                <strong>{{ $errors->first('category') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Head</label>
                                        <div class="col-md-7">
                                            <input type="text" class="form-control col-md-7 col-xs-12"
                                                   placeholder="Head" name="head"
                                                   value="{{$expanse->head}}">
                                            @if ($errors->has('head'))
                                                <span class="help-block">
                                                <strong>{{ $errors->first('head') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Amount</label>
                                        <div class="col-md-7">
                                            <input type="text" class="form-control col-md-7 col-xs-12"
                                                   placeholder="Amount" name="amount"
                                                   value="{{$expanse->amount}}">
                                            @if ($errors->has('amount'))
                                                <span class="help-block">
                                                <strong>{{ $errors->first('amount') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>


                                    <div class="form-actions right">
                                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                            <button type="submit" class="btn btn-success">Submit</button>
                                            <a type="cancel" href="{{ url('dashboard') }}" class="btn btn-primary">Cancel</a>
                                        </div>
                                    </div>
                                </div>
                            </form>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection