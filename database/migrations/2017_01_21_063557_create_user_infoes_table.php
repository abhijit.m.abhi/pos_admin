<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserInfoesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_infoes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 250);
            $table->string('user_name', 250);
            $table->string('password', 50);
            $table->integer('user_type_id');
            $table->string('email', 250);
            $table->string('phone', 250);
            $table->string('remark', 250);
            $table->dateTime('last_changed');
            $table->integer('last_changedBy');
            $table->integer('syncSession_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_infoes');
    }
}
