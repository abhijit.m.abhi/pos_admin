<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 255);
            $table->integer('category_id')->unsigned();
            $table->integer('unit_id')->unsigned();
            $table->string('manufacturer', 250);
            $table->decimal('discount', 18, 2);
            $table->string('remark', 250);
            $table->dateTime('last_changed');
            $table->integer('last_changedBy');
            $table->integer('syncSession_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
