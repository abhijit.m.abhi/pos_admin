<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductBuyDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_buy_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_buy_id')->unsigned();
            $table->integer('product_id')->unsigned();
            $table->integer('quantity');
            $table->integer('sale_quantity');
            $table->integer('bonus_quantity');
            $table->decimal('buy_price', 18, 2);
            $table->decimal('dealer_price', 18, 2);
            $table->decimal('sale_price', 18, 2);
            $table->dateTime('date');
            $table->string('bar_code', 250);
            $table->string('remark', 250);
            $table->dateTime('last_changed');
            $table->integer('last_changedBy');
            $table->integer('syncSession_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_buy_details');
    }
}
