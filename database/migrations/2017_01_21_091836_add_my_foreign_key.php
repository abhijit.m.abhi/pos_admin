<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMyForeignKey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('categories', function (Blueprint $table) {

            $table->foreign('parent_id')->references('id')->on('categories')->onDelete('cascade');;

        });

        Schema::table('products', function (Blueprint $table) {

            $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');

        });

        Schema::table('products', function (Blueprint $table) {

            $table->foreign('unit_id')->references('id')->on('units')->onDelete('cascade');

        });

        Schema::table('product_buys', function (Blueprint $table) {

            $table->foreign('supplier_id')->references('id')->on('suppliers')->onDelete('cascade');

        });

        Schema::table('product_buy_details', function (Blueprint $table) {

            $table->foreign('product_buy_id')->references('id')->on('product_buys')->onDelete('cascade');

        });

        Schema::table('product_buy_details', function (Blueprint $table) {

            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');

        });

        Schema::table('user_permissions', function (Blueprint $table) {

            $table->foreign('user_id')->references('id')->on('user_infoes');

        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('categories', function (Blueprint $table) {

            $table->dropForeign('categories_parent_id_foreign');

        });

        Schema::table('products', function (Blueprint $table) {

            $table->dropForeign('products_category_id_foreign');

        });

        Schema::table('products', function (Blueprint $table) {

            $table->dropForeign('products_unit_id_foreign');

        });

        Schema::table('product_buys', function (Blueprint $table) {

            $table->dropForeign('product_buys_supplier_id_foreign');

        });

        Schema::table('product_buy_details', function (Blueprint $table) {

            $table->dropForeign('product_buy_details_product_buy_id_foreign');

        });

        Schema::table('product_buy_details', function (Blueprint $table) {

            $table->dropForeign('product_buy_details_product_id_foreign');

        });

        Schema::table('user_permissions', function (Blueprint $table) {

            $table->dropForeign('user_permissions_user_id_foreign');

        });
    }


}
