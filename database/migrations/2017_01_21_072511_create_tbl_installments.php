<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblInstallments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('loan_installments',function (Blueprint $table){

            $table->increments('id');
            $table->double('deposit_amount')->nullable();
            $table->double('interest')->nullable();
            $table->string('bank_branch')->nullable();
            $table->string('deposited_by')->nullable();
            $table->integer('loan_id')->unsigned()->nullable();
            $table->date('date')->nullable();
            $table->integer('saved_by')->nullable();
            $table->timestamps();

            $table->foreign('loan_id')
                ->references('id')->on('tbl_loan')
                ->onDelete('cascade');

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('loan_installments');
    }
}
