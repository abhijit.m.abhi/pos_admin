<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLoanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('tbl_loan',function (Blueprint $table){

            $table->increments('id');
            $table->string('account_no')->nullable();
            $table->string('account_name')->nullable();
            $table->string('bank_name')->nullable();
            $table->string('branch_name')->nullable();
            $table->double('bank_balance')->nullable();
            $table->date('loan_start_date')->nullable()->format('m-d-Y');
            $table->string('loan_duration')->nullable();
            $table->double('percent')->nullable();
            $table->double('amount')->nullable();
            $table->date('date')->nullable();
            $table->integer('saved_by')->nullable();
            $table->string('comment')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //hjhvcjh
        Schema::drop('tbl_loan');
    }
}
