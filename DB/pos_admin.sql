-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 21, 2017 at 12:00 PM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 7.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pos_admin`
--

-- --------------------------------------------------------

--
-- Table structure for table `budgetcategory`
--

CREATE TABLE `budgetcategory` (
  `id` int(10) UNSIGNED NOT NULL,
  `categoryName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `budgets`
--

CREATE TABLE `budgets` (
  `id` int(10) UNSIGNED NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `category` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `amount` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `extend_amount` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `comment` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `saved_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `parent_id` int(10) UNSIGNED NOT NULL,
  `remark` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `last_changed` datetime NOT NULL,
  `last_changedBy` int(11) NOT NULL,
  `syncSession_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `expansecategory`
--

CREATE TABLE `expansecategory` (
  `id` int(10) UNSIGNED NOT NULL,
  `categoryName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `expansecategory`
--

INSERT INTO `expansecategory` (`id`, `categoryName`, `created_at`, `updated_at`) VALUES
(75, 'New Cat', '2017-01-19 04:56:28', '2017-01-19 04:56:28'),
(76, 'New data', '2017-01-19 05:00:32', '2017-01-19 05:00:32'),
(77, 'New Data 2', '2017-01-19 05:00:50', '2017-01-19 05:00:50'),
(78, 'New Data 3', '2017-01-19 05:01:04', '2017-01-19 05:01:04'),
(79, 'New Data 4', '2017-01-19 05:01:15', '2017-01-19 05:01:15'),
(80, 'New Data 5', '2017-01-19 05:06:52', '2017-01-19 05:06:52'),
(81, 'New Data 6', '2017-01-19 05:07:49', '2017-01-19 05:07:49'),
(82, 'New Data 7', '2017-01-19 05:07:57', '2017-01-19 05:07:57'),
(83, 'New Data 8', '2017-01-19 05:31:33', '2017-01-19 05:31:33'),
(84, 'Shuvo vai', '2017-01-19 05:31:48', '2017-01-19 05:31:48');

-- --------------------------------------------------------

--
-- Table structure for table `expanses`
--

CREATE TABLE `expanses` (
  `id` int(10) UNSIGNED NOT NULL,
  `category` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `head` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `amount` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `saved_by` int(11) NOT NULL,
  `date` date NOT NULL,
  `flag` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `expanses`
--

INSERT INTO `expanses` (`id`, `category`, `head`, `amount`, `saved_by`, `date`, `flag`, `created_at`, `updated_at`) VALUES
(30, 'staff_salary', 'Abul', '58000', 4, '2017-01-18', 1, '2017-01-18 02:30:56', '2017-01-18 02:30:56'),
(31, 'utility', 'Bill', '10000', 4, '2017-01-18', 1, '2017-01-18 02:33:13', '2017-01-18 02:33:13'),
(32, 'transport', 'Bus', '5000', 4, '2017-01-18', 1, '2017-01-18 02:35:20', '2017-01-18 02:35:20'),
(33, 'val1', 'ABD', '50000', 4, '2017-01-18', 1, '2017-01-18 02:56:49', '2017-01-18 02:56:49'),
(34, 'Monthly Expense', 'dfgt', '123', 4, '2017-01-18', 1, '2017-01-18 04:58:01', '2017-01-18 04:58:01'),
(35, 'Staff Salary', 'Tabil', '9000', 4, '2017-01-18', 1, '2017-01-18 05:08:27', '2017-01-18 05:08:27'),
(36, 'btv', 'TV', '10000', 4, '2017-01-19', 1, '2017-01-19 03:28:49', '2017-01-19 03:28:49'),
(37, 'Tea breakkk', 'Coffee', '100', 4, '2017-01-19', 1, '2017-01-19 04:38:27', '2017-01-19 04:38:27'),
(38, 'New data', 'Building', '80000', 4, '2017-01-20', 1, '2017-01-20 01:57:15', '2017-01-20 01:57:15');

-- --------------------------------------------------------

--
-- Table structure for table `incomes`
--

CREATE TABLE `incomes` (
  `id` int(10) UNSIGNED NOT NULL,
  `date` date NOT NULL,
  `head` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `amount` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `saved_by` int(11) NOT NULL,
  `flag` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `incomes`
--

INSERT INTO `incomes` (`id`, `date`, `head`, `amount`, `saved_by`, `flag`, `created_at`, `updated_at`) VALUES
(1, '2017-01-20', 'Sell', '8000', 4, 1, '2017-01-20 03:44:21', '2017-01-20 03:44:21'),
(2, '2017-01-20', 'Customer', '1000', 4, 1, '2017-01-20 03:45:41', '2017-01-20 03:45:41'),
(4, '2017-01-20', 'Artd', '80000', 4, 1, '2017-01-20 04:37:33', '2017-01-20 04:37:33'),
(5, '2017-01-20', 'Tea', '7000', 4, 1, '2017-01-20 04:37:56', '2017-01-20 04:37:56'),
(6, '2017-01-20', 'Bonus', '78000', 4, 1, '2017-01-20 05:05:26', '2017-01-20 05:05:26');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(3, '2014_10_12_000000_create_users_table', 1),
(4, '2014_10_12_100000_create_password_resets_table', 1),
(6, '2017_01_11_040553_create_loan_table', 3),
(8, '2017_01_11_053808_create_expanse_table', 4),
(9, '2017_01_18_101347_create_expanseCategory_table', 5),
(10, '2017_01_20_082256_create_incomes_table', 6),
(11, '2017_01_20_084555_create_budgets_table', 7),
(12, '2017_01_20_090921_create_budgetCategory_table', 7),
(27, '2017_01_21_055725_create_categories_table', 8),
(28, '2017_01_21_061101_create_products_table', 8),
(29, '2017_01_21_061812_create_product_buys_table', 8),
(30, '2017_01_21_062218_create_product_buy_details_table', 8),
(31, '2017_01_21_062951_create_suppliers_table', 8),
(32, '2017_01_21_063230_create_units_table', 8),
(33, '2017_01_21_063557_create_user_infoes_table', 8),
(44, '2017_01_21_063932_create_user_permissions_table', 9),
(56, '2017_01_21_091836_add_my_foreign_key', 10);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `unit_id` int(10) UNSIGNED NOT NULL,
  `manufacturer` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `discount` decimal(18,2) NOT NULL,
  `remark` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `last_changed` datetime NOT NULL,
  `last_changedBy` int(11) NOT NULL,
  `syncSession_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product_buys`
--

CREATE TABLE `product_buys` (
  `id` int(10) UNSIGNED NOT NULL,
  `supplier_id` int(10) UNSIGNED NOT NULL,
  `total` decimal(18,2) NOT NULL,
  `discount` decimal(18,2) NOT NULL,
  `date` datetime NOT NULL,
  `remark` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `last_changed` datetime NOT NULL,
  `last_changedBy` int(11) NOT NULL,
  `syncSession_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product_buy_details`
--

CREATE TABLE `product_buy_details` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_buy_id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `quantity` int(11) NOT NULL,
  `sale_quantity` int(11) NOT NULL,
  `bonus_quantity` int(11) NOT NULL,
  `buy_price` decimal(18,2) NOT NULL,
  `dealer_price` decimal(18,2) NOT NULL,
  `sale_price` decimal(18,2) NOT NULL,
  `date` datetime NOT NULL,
  `bar_code` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `remark` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `last_changed` datetime NOT NULL,
  `last_changedBy` int(11) NOT NULL,
  `syncSession_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `suppliers`
--

CREATE TABLE `suppliers` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `remark` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `last_changed` datetime NOT NULL,
  `last_changedBy` int(11) NOT NULL,
  `syncSession_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_loan`
--

CREATE TABLE `tbl_loan` (
  `id` int(10) UNSIGNED NOT NULL,
  `account_no` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `account_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bank_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `branch_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bank_balance` double DEFAULT NULL,
  `loan_start_date` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `loan_duration` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `percent` double DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `date` date DEFAULT NULL,
  `saved_by` int(11) DEFAULT NULL,
  `comment` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_loan`
--

INSERT INTO `tbl_loan` (`id`, `account_no`, `account_name`, `bank_name`, `branch_name`, `bank_balance`, `loan_start_date`, `loan_duration`, `percent`, `amount`, `date`, `saved_by`, `comment`, `created_at`, `updated_at`) VALUES
(8, 'R-05', '4564564', 'DBBL', '46', NULL, '2017-01-18', '46 years 4 months 6 days', 46, 46, '2017-01-18', 4, '46464', '2017-01-18 00:14:02', '2017-01-18 00:14:02');

-- --------------------------------------------------------

--
-- Table structure for table `units`
--

CREATE TABLE `units` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `remark` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `last_changed` datetime NOT NULL,
  `last_changedBy` int(11) NOT NULL,
  `syncSession_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `fname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` longtext COLLATE utf8_unicode_ci NOT NULL,
  `photo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'user',
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'active',
  `api_token` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `saved_by` int(11) DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `fname`, `lname`, `email`, `password`, `address`, `photo`, `type`, `status`, `api_token`, `saved_by`, `remember_token`, `created_at`, `updated_at`) VALUES
(4, 'Abhijit Mondal', 'Abhi', 'abhi.inc@live.com', '$2y$10$hyYbJJMFY7xIkDJV9s8Vg.ZbSufcpBAvYzoIBhtVstqVZNAgmlMvy', 'Mohammadpur, Dhaka.', 'Abhijit Mondal60910.jpg', 'admin', 'active', 'qleog4qolM9KrHH7GkShfvSgAXg66Oku6v3FCgx2rqgddTEEUBXkuEHwuw4b', 2, 'XCu3twRstbb1B4R9ZZvLK8CVuZAbspNBpOjwOV27Z1bR38DNljMeZgVCTABL', '2017-01-09 04:25:53', '2017-01-20 22:27:52'),
(14, 'Kamruzzaman', 'Kh', 'rubel@sfs.com', '$2y$10$p1CreckSGLjODg7aEDyKTO7SZ/soeacBTpLBRKQC/V8RXv3HD5Kuy', 'Banassree, Dhaka.', 'Kamruzzaman86558.png', 'admin', 'active', 'LkkcLpCUHvV26E2WTYFcMeErp2j2q4mJBqzo9QOwIiXtFqOJiEHcXh9NaPVo', 4, 'TTbjKHko9Px86rNzK7NWXPBgqKokFf1wCq6x0aHru41c8mb6DUnWUzUpL7lc', '2017-01-18 02:21:40', '2017-01-20 01:32:24');

-- --------------------------------------------------------

--
-- Table structure for table `user_infoes`
--

CREATE TABLE `user_infoes` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `user_name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `user_type_id` int(11) NOT NULL,
  `email` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `remark` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `last_changed` datetime NOT NULL,
  `last_changedBy` int(11) NOT NULL,
  `syncSession_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user_permissions`
--

CREATE TABLE `user_permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `permission_no` int(11) NOT NULL,
  `last_changed` datetime NOT NULL,
  `last_changedBy` int(11) NOT NULL,
  `syncSession_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `budgetcategory`
--
ALTER TABLE `budgetcategory`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `budgetcategory_categoryname_unique` (`categoryName`);

--
-- Indexes for table `budgets`
--
ALTER TABLE `budgets`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `categories_parent_id_foreign` (`parent_id`);

--
-- Indexes for table `expansecategory`
--
ALTER TABLE `expansecategory`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `expansecategory_categoryname_unique` (`categoryName`);

--
-- Indexes for table `expanses`
--
ALTER TABLE `expanses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `incomes`
--
ALTER TABLE `incomes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `products_category_id_foreign` (`category_id`),
  ADD KEY `products_unit_id_foreign` (`unit_id`);

--
-- Indexes for table `product_buys`
--
ALTER TABLE `product_buys`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_buys_supplier_id_foreign` (`supplier_id`);

--
-- Indexes for table `product_buy_details`
--
ALTER TABLE `product_buy_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_buy_details_product_buy_id_foreign` (`product_buy_id`),
  ADD KEY `product_buy_details_product_id_foreign` (`product_id`);

--
-- Indexes for table `suppliers`
--
ALTER TABLE `suppliers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_loan`
--
ALTER TABLE `tbl_loan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `units`
--
ALTER TABLE `units`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD UNIQUE KEY `users_api_token_unique` (`api_token`);

--
-- Indexes for table `user_infoes`
--
ALTER TABLE `user_infoes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_permissions`
--
ALTER TABLE `user_permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_permissions_user_id_foreign` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `budgetcategory`
--
ALTER TABLE `budgetcategory`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `budgets`
--
ALTER TABLE `budgets`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `expansecategory`
--
ALTER TABLE `expansecategory`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=85;
--
-- AUTO_INCREMENT for table `expanses`
--
ALTER TABLE `expanses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;
--
-- AUTO_INCREMENT for table `incomes`
--
ALTER TABLE `incomes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `product_buys`
--
ALTER TABLE `product_buys`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `product_buy_details`
--
ALTER TABLE `product_buy_details`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `suppliers`
--
ALTER TABLE `suppliers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_loan`
--
ALTER TABLE `tbl_loan`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `units`
--
ALTER TABLE `units`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `user_infoes`
--
ALTER TABLE `user_infoes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user_permissions`
--
ALTER TABLE `user_permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `categories`
--
ALTER TABLE `categories`
  ADD CONSTRAINT `categories_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `products_unit_id_foreign` FOREIGN KEY (`unit_id`) REFERENCES `units` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `product_buys`
--
ALTER TABLE `product_buys`
  ADD CONSTRAINT `product_buys_supplier_id_foreign` FOREIGN KEY (`supplier_id`) REFERENCES `suppliers` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `product_buy_details`
--
ALTER TABLE `product_buy_details`
  ADD CONSTRAINT `product_buy_details_product_buy_id_foreign` FOREIGN KEY (`product_buy_id`) REFERENCES `product_buys` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `product_buy_details_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `user_permissions`
--
ALTER TABLE `user_permissions`
  ADD CONSTRAINT `user_permissions_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `user_infoes` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
